//
//  iLioUITests.swift
//  iLioUITests
//
//  Created by Dimitris Delis on 14/6/21.
//

import XCTest

class iLioUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLogin() throws {

        let app = XCUIApplication()
        app.launch()
        let usernameTextfield = app.textFields["Username"]
        let passwordTextfield = app.secureTextFields["Password"]

        XCTAssert(usernameTextfield.placeholderValue == "Username")
        XCTAssert(passwordTextfield.placeholderValue == "Password")

    }
}
