//
//  CategoryEndpointProtocol.swift
//  iLio
//
//  Created by Dimitris Delis on 16/6/21.
//

import Foundation

/// Category Endpoint Protocol
protocol CategoryEndpointProtocol {

    /// Every enum type will return an endpoint
    /// - Returns: can't be nil
    func getDescription() -> String
}
