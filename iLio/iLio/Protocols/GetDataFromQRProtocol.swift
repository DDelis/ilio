//
//  GetDataFromQRProtocol.swift
//  iLio
//
//  Created by Dimitris Delis on 9/7/21.
//

import Foundation

protocol GetDataFromQRProtocol: AnyObject {
    func useData(code: String)
}
