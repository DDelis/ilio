//
//  CharacterCreationCellProtocol.swift
//  iLio
//
//  Created by Dimitris Delis on 29/6/21.
//

import Foundation
import UIKit

protocol CharacterCreationCellProtocol {
    func getValue() -> String?
    func hideKeyboard()
}
