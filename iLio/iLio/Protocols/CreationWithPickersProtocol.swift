//
//  CreationWithPickersProtocol.swift
//  iLio
//
//  Created by Dimitris Delis on 7/7/21.
//

import Foundation
import UIKit

protocol CreationWithPickersProtocol : UIViewController {
    
    func tapped()
}
