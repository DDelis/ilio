//
//  BattlefieldPresentableProtocol.swift
//  iLio
//
//  Created by Dimitris Delis on 5/7/21.
//

import Foundation
import UIKit

protocol BattlefieldPresentableProtocol {

    var position:Position { get set }
    var delegate: GenericBattleCollectionViewCell? { get set }
    var type: FieldType { get set }
    var image: String? { get set }
    func getSelected()
}
