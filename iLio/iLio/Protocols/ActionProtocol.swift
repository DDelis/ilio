//
//  ActionProtocol.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import Foundation
import UIKit

/// Every Action Must comform to this protocol
protocol ActionProtocol {

    /// The image for this action, can be nil
    var image: UIImage? { get set }

    /// The action's title
    var title: String { get set }

    /// It's delegate
    var delegate: StandardViewController { get set }

    /// What happens when this action is pressed
    func actionPressed()
}

protocol SettingsActionProtocol : ActionProtocol {
}
