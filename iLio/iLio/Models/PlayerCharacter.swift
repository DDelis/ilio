//
//  PlayerCharacter.swift
//  iLio
//
//  Created by Dimitris Delis on 28/6/21.
//

import Foundation
import UIKit

struct PlayerCharacter: Codable, Equatable {

    static func == (lhs: PlayerCharacter, rhs: PlayerCharacter) -> Bool {
        return lhs.name == rhs.name
    }

    /// Simple String for character name
    var name: String?

    /// Enumeration for character's sex
    var sex: PlayerCharacterSex?

    /// The Race of the character
    var race: String? //Race?

    /// The Class of the character
    var characterClass: String? //Class?

    /// Level of the character
    var level: Level?

    /// Background of the character
    var background: String?

    /// Stats of the character
    var stats: Stats

    /// Gets the UIImage of the player character
    /// - Returns: cannot be nil
    func getQRImage() -> UIImage? {
        guard let data = UserDefaultManager.encodePlayerCharacters([self]) else { return nil }
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        return nil
    }

}

struct Stats: Codable {

    /// strenght
    var STR: Int = 0

    /// dexterity
    var DEX: Int = 0

    /// constitution
    var CON: Int = 0

    /// intelligence
    var INT: Int = 0

    /// wisdom
    var WIS: Int = 0

    /// charisma
    var CHA: Int = 0

    init(STR: Int, DEX: Int, CON: Int, INT: Int, CHA: Int, WIS: Int) {
        self.STR = STR
        self.DEX = DEX
        self.CON = CON
        self.INT = INT
        self.CHA = CHA
        self.WIS = WIS
    }
    
    init (random: Bool) {
        if random {
            self.STR = Int.random(in: 0...5)
            self.DEX = Int.random(in: 0...5)
            self.CON = Int.random(in: 0...5)
            self.INT = Int.random(in: 0...5)
            self.CHA = Int.random(in: 0...5)
            self.WIS = Int.random(in: 0...5)
        }
    }

}

/// A party of characters
struct CharacterParty: Codable, Equatable {

    static func == (lhs: CharacterParty, rhs: CharacterParty) -> Bool {
        return lhs.id == rhs.id
    }

    var id: String

    var players: [PlayerCharacter]

    init(id: String, players: [PlayerCharacter]) {
        self.id = id
        self.players = players
    }
}

