//
//  Category.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import Foundation

struct Category: Codable {
    
    var count:Double
    var results:[Subcategory]
}

struct Subcategory: Codable {
    
    var index: String
    var name: String
    var url: String?
    
}
