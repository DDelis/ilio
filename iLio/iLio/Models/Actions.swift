//
//  Actions.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import Foundation
import UIKit


/// Sample Demo Action Used For Debugging
struct DemoAction : ActionProtocol {
    
    var image: UIImage?
    var title: String
    var delegate: StandardViewController
    
    func actionPressed() {
        print("\(self) Action Pressed")
        self.delegate.present(AlertMaker.makeErrorAlert(.NotImplementedYet), animated: true, completion: nil)
    }
}

/// The Add Action
struct AddAction: ActionProtocol {
    var image: UIImage?
    var title: String = "Add character"
    var delegate: StandardViewController
    
    func actionPressed() {
        let vc = ListOfCharactersViewController()
        self.delegate.navigationController?.pushViewController(vc, animated: true)
    }
    
    init(delegate: StandardViewController) {
        self.image = UIImage(systemName: "plus")
        self.delegate = delegate
    }
}

struct AddBattlefieldAction: ActionProtocol {
    var image: UIImage?
    var title: String  = "Add a battlefield"
    var delegate: StandardViewController

    func actionPressed() {
        let vc = AddBattlefieldViewController()
        self.delegate.navigationController?.pushViewController(vc, animated: true)
    }

    init(delegate: StandardViewController) {
        self.image = UIImage(systemName: "plus")
        self.delegate = delegate
    }
}

struct SettingsAction: ActionProtocol {
    var image: UIImage?
    var title: String = "Settings"
    var delegate: StandardViewController

    func actionPressed() {
        let vc = SettingsViewController()
        self.delegate.navigationController?.pushViewController(vc, animated: true)
    }

    init(delegate: StandardViewController) {
        self.delegate = delegate
        self.image = UIImage(named: "equipment")
    }
}

struct BattlefieldListAction: ActionProtocol {

    var image: UIImage?
    var title: String = "Battlefields"
    var delegate: StandardViewController

    func actionPressed() {
        let vc = BattlefieldListViewController()
        self.delegate.navigationController?.pushViewController(vc, animated: true)
    }

    init(delegate: StandardViewController) {
        self.image = UIImage(systemName: "book.fill")
        self.delegate = delegate
    }
}

struct PartyManagerAction: ActionProtocol {

    var image: UIImage?
    var title: String = "Create A Party"
    var delegate: StandardViewController

    func actionPressed() {
        let vc = PartyManagerViewController()
        self.delegate.navigationController?.pushViewController(vc, animated: true)
    }

    init(delegate: StandardViewController) {
        self.image = UIImage(systemName: "person.3.fill")
        self.delegate = delegate
    }
}

struct ListOfPartiesAction: ActionProtocol {

    var image: UIImage?
    var title: String = "Parties List"
    var delegate: StandardViewController

    func actionPressed() {
        let vc = ListOfPartiesViewController()
        self.delegate.navigationController?.pushViewController(vc, animated: true)
    }

    init(delegate: StandardViewController) {
        self.image = UIImage(systemName: "person.3")
        self.delegate = delegate
    }
}

struct ViewEncyclopediaAction: ActionProtocol {
    
    var image: UIImage?
    var title: String = "Encyclopedia"
    var delegate: StandardViewController
    
    func actionPressed() {
        let vc = EncyclopediaViewController()
        self.delegate.navigationController?.pushViewController(vc, animated: true)
    }
    
    init(delegate: StandardViewController) {
        self.image = UIImage(systemName: "book")
        self.delegate = delegate
    }
}

struct EncyclopediaCategoryAction: ActionProtocol {
    
    var image: UIImage?
    var title: String
    var delegate: StandardViewController
    var type: CategoryType
    
    func actionPressed() {
        
        let vc = CategoryViewController()
        switch type {
        case .Monsters:
            vc.type = .Monsters
        case .Proficiencies:
            vc.type = .Proficiencies
        case .Backgrounds:
            vc.type = .Backgrounds
        case .Classes:
            vc.type = .Classes
        case .Races:
            vc.type = .Races
        case .Equipment:
            vc.type = .Equipment
        case .Spells:
            vc.type = .Spells
        case .GameMechanics:
            vc.type = .GameMechanics
        case .Rules:
            vc.type = .Rules
        default:
            break
        }
        self.delegate.navigationController?.pushViewController(vc, animated: true)
    }
}

struct SubCategoryAction: ActionProtocol {
    
    var image: UIImage?
    var title: String
    var type: ContentType
    var delegate: StandardViewController
    var url: String
    
    
    func actionPressed() {
        let vc = CategoryDetailsViewController()
        vc.contentUrl = self.url
        vc.type = self.type
        vc.navigationItem.title = self.title
        self.delegate.navigationController?.pushViewController(vc, animated: true)
    }
}


struct ResetDataAction: SettingsActionProtocol {

    var image: UIImage?
    var title: String
    var delegate: StandardViewController

    func actionPressed() {
        let action = UIAlertAction(title: "Confirm", style: .destructive) { _ in
            UserDefaultManager.deleteMapsFromUserDefaults()
            UserDefaultManager.deletePartiesFromUserDefaults()
            UserDefaultManager.deleteAllCharactersFromUsersDefaults()
        }
        self.delegate.present(AlertMaker.makeConfirmAlert(.confirmDeleteUserDefaultsAll, action: action), animated: true, completion: nil)
    }

    init (delegate: StandardViewController) {
        self.delegate = delegate
        self.title = "Delete All Data"
        self.image = UIImage(systemName: "trash")
    }
    
}
