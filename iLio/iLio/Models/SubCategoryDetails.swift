//
//  SubCategoryDetails.swift
//  iLio
//
//  Created by Dimitris Delis on 15/6/21.
//

import Foundation

// MARK: - Rule
struct Rule: Codable {
    let name, index, desc: String?
    let subsections: [Subsection]?
    let url: String?
}

// MARK: - Subsection
// also used as generic response
struct Subsection: Codable {
    let name, index, url: String?
}

// MARK: - RuleSection
struct RuleSection: Codable {
    let name, index, desc, url: String?
}

// MARK: - GameMechanic
struct GameMechanic: Codable {
    let index, name: String?
    let desc: [String]?
    let url: String?
}

// MARK: - DamageType
struct DamageType: Codable {
    let index, name: String?
    let desc: [String]?
    let url: String?
}

// MARK: - MagicSchool
struct MagicSchool: Codable {
    let index, name, desc, url: String?
}

// MARK: - Monster
struct Monster: Codable {
    let index, name, size, type: String?
    let subtype: String?
    let alignment: String?
    let armorClass, hitPoints: Double?
    let hitDice: String?
    let speed: Speed?
    let strength, dexterity, constitution, intelligence: Double?
    let wisdom, charisma: Double?
    let proficiencies: [ProficiencyElement]?
    let damageVulnerabilities, conditionImmunities: [DcTypeClass]?
    let damageImmunities, damageResistances: [String]?
    let senses: Senses?
    let languages: String?
    let challengeRating, xp: Double?
    let specialAbilities: [SpecialAbility]?
    let actions: [Action]?
    let legendaryActions: [LegendaryAction]?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index, name, size, type, subtype, alignment
        case armorClass = "armor_class"
        case hitPoints = "hit_points"
        case hitDice = "hit_dice"
        case speed, strength, dexterity, constitution, intelligence, wisdom, charisma, proficiencies
        case damageVulnerabilities = "damage_vulnerabilities"
        case damageResistances = "damage_resistances"
        case damageImmunities = "damage_immunities"
        case conditionImmunities = "condition_immunities"
        case senses, languages
        case challengeRating = "challenge_rating"
        case xp
        case specialAbilities = "special_abilities"
        case actions
        case legendaryActions = "legendary_actions"
        case url
    }
}

// MARK: - Action
struct Action: Codable {
    let name, desc: String?
    let options: Options?
    let damage: [MonsterDamage]?
    let attackBonus: Int?
    let dc: Dc?
    let usage: Usage?

    enum CodingKeys: String, CodingKey {
        case name, desc, options, damage
        case attackBonus = "attack_bonus"
        case dc, usage
    }
}

// MARK: - MonsterDamage
struct MonsterDamage: Codable {
    let damageType: DcTypeClass?
    let damageDice: String?

    enum CodingKeys: String, CodingKey {
        case damageType = "damage_type"
        case damageDice = "damage_dice"
    }
}

// MARK: - DcTypeClass
struct DcTypeClass: Codable {
    let index, name, url: String?
}

// MARK: - Dc
struct Dc: Codable {
    let dcType: DcTypeClass?
    let dcValue: Int?
    let successType: String?

    enum CodingKeys: String, CodingKey {
        case dcType = "dc_type"
        case dcValue = "dc_value"
        case successType = "success_type"
    }
}

// MARK: - Options
struct Options: Codable {
    let choose: Int?
    let from: [[From]]?
}

// MARK: - From
struct From: Codable {
    let name: String?
    let count: Int?
    let type: String?
}

// MARK: - Usage
struct Usage: Codable {
    let type: String?
    let times: Int?
}

// MARK: - LegendaryAction
struct LegendaryAction: Codable {
    let name, desc: String?
    let attackBonus: Int?
    let damage: [MonsterDamage]?

    enum CodingKeys: String, CodingKey {
        case name, desc
        case attackBonus = "attack_bonus"
        case damage
    }
}

// MARK: - ProficiencyElement
struct ProficiencyElement: Codable {
    let value: Int?
    let proficiency: DcTypeClass?
}

// MARK: - Senses
struct Senses: Codable {
    let darkvision: String?
    let passivePerception: Int?

    enum CodingKeys: String, CodingKey {
        case darkvision
        case passivePerception = "passive_perception"
    }
}

// MARK: - SpecialAbility
struct SpecialAbility: Codable {
    let name, desc: String?
    let dc: Dc?
}

// MARK: - Speed
struct Speed: Codable {
    let walk, fly, swim: String?
}

// MARK: - Spell
struct Spell: Codable {
    let index, name: String?
    let desc, higherLevel: [String]?
    let range: String?
    let components: [String]?
    let material: String?
    let ritual: Bool?
    let duration: String?
    let concentration: Bool?
    let castingTime: String?
    let dc: DC?
    let level: Int?
    let attackType: String?
    let damage: SpellDamage?
    let school: School?
    let classes, subclasses: [School]?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index, name, desc, dc
        case higherLevel = "higher_level"
        case range, components, material, ritual, duration, concentration
        case castingTime = "casting_time"
        case level
        case attackType = "attack_type"
        case damage, school, classes, subclasses, url
    }
}

// MARK: - DC
struct DC: Codable {
    let dcType: School?
    let dcSuccess: String?

    enum CodingKeys: String, CodingKey {
        case dcType = "dc_type"
        case dcSuccess = "dc_success"
    }
}

// MARK: - School
struct School: Codable {
    let index, name, url: String?
}

// MARK: - SpellDamage
struct SpellDamage: Codable {
    let damageType: School?
    let damageAtCharacterLevel: [String: String]?

    enum CodingKeys: String, CodingKey {
        case damageType = "damage_type"
        case damageAtCharacterLevel = "damage_at_character_level"
    }
}

// MARK: - WeaponProperty
struct WeaponProperty: Codable {
    let index, name: String?
    let desc: [String]?
    let url: String?
}

// MARK: - MagicItem
struct MagicItem: Codable {
    let index, name: String?
    let equipmentCategory: EquipmentCategory?
    let desc: [String]?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index, name
        case equipmentCategory = "equipment_category"
        case desc, url
    }
}

// MARK: - EquipmentPack
struct EquipmentPack: Codable {
    let index, name: String?
    let equipmentCategory, gearCategory: EquipmentCategory?
    let cost: Cost?
    let contents: [Content]?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index, name
        case equipmentCategory = "equipment_category"
        case gearCategory = "gear_category"
        case cost, contents, url
    }
}

// MARK: - Content
struct Content: Codable {
    let item: EquipmentCategory?
    let quantity: Int?
}

// MARK: - EquipmentCategory
struct EquipmentCategory: Codable {
    let index, name: String?
    let equipment: [EquipmentItem]?
    let url: String?
}

// MARK: - Cost
struct Cost: Codable {
    let quantity: Int?
    let unit: String?
}

// MARK: - AdventurePack
struct AdventurePack: Codable {
    let index, name: String?
    let equipmentCategory, gearCategory: EquipmentCategory?
    let cost: Cost?
    let contents: [Content]?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index, name
        case equipmentCategory = "equipment_category"
        case gearCategory = "gear_category"
        case cost, contents, url
    }
}

// MARK: - EquipmentItem
struct EquipmentItem: Codable {
    let index, name: String?
    let desc: [String]?
    let equipmentCategory: EquipmentCategory?
    let weaponCategory, weaponRange, categoryRange: String?
    let range: Range?
    let armorCategory: String?
    let armorClass: ArmorClass?
    let damage: WeaponDamage?
    let strMinimum: Int?
    let properties: [EquipmentCategory]?
    let stealthDisadvantage: Bool?
    let weight: Int?
    let cost: Cost?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index, name, desc
        case equipmentCategory = "equipment_category"
        case armorCategory = "armor_category"
        case armorClass = "armor_class"
        case weaponCategory = "weapon_category"
        case weaponRange = "weapon_range"
        case categoryRange = "category_range"
        case strMinimum = "str_minimum"
        case stealthDisadvantage = "stealth_disadvantage"
        case weight, cost, url, range, properties, damage
    }
}

// MARK: - ArmorClass
struct ArmorClass: Codable {
    let base: Int?
    let dexBonus: Bool?
    let maxBonus: Bool?

    enum CodingKeys: String, CodingKey {
        case base
        case dexBonus = "dex_bonus"
        case maxBonus = "max_bonus"
    }
}


// MARK: - Range
struct Range: Codable {
    let normal: Int?
    let long: Int?
}

// MARK: - WeaponDamage
struct WeaponDamage: Codable {
    let damageDice: String?
    let damageType: EquipmentCategory?

    enum CodingKeys: String, CodingKey {
        case damageDice = "damage_dice"
        case damageType = "damage_type"
    }
}

// MARK: - Race
struct Race: Codable {
    let index, name: String?
    let speed: Int?
    let abilityBonuses: [AbilityBonus]?
    let alignment, age, size, sizeDescription: String?
    let startingProficiencies: [Language]?
    let startingProficiencyOptions: StartingProficiencyOptions?
    let languages: [Language]?
    let languageDesc: String?
    let traits, subraces: [Language]?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index, name, speed
        case abilityBonuses = "ability_bonuses"
        case alignment, age, size
        case sizeDescription = "size_description"
        case startingProficiencies = "starting_proficiencies"
        case startingProficiencyOptions = "starting_proficiency_options"
        case languages
        case languageDesc = "language_desc"
        case traits, subraces, url
    }
}

// MARK: - AbilityBonus
struct AbilityBonus: Codable {
    let abilityScore: Language?
    let bonus: Int?

    enum CodingKeys: String, CodingKey {
        case abilityScore = "ability_score"
        case bonus
    }
}

// MARK: - Language
struct Language: Codable {
    let index, name, url: String?
}

// MARK: - StartingProficiencyOptions
struct StartingProficiencyOptions: Codable {
    let from: [Language]?
    let choose: Int?
    let type: String?
}

// MARK: - SubRaces
struct SubRaces: Codable {
    let count: Int?
    let results: [SubRace]?
}

// MARK: - Proficiencies
struct Proficiencies: Codable {
    let count: Int?
    let results: [Proficiency]?
}

// MARK: - Traits
struct Traits: Codable {
    let count: Int?
    let results: [Trait]?
}

// MARK: - SubRace
struct SubRace: Codable {
    let index, name: String?
    let race: Race?
    let desc: String?
    let abilityBonuses: [AbilityBonus]?
    let startingProficiencies: [Proficiency]?
    let languages: [Language]?
    let racialTraits: [Race]?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index, name, race, desc
        case abilityBonuses = "ability_bonuses"
        case startingProficiencies = "starting_proficiencies"
        case languages
        case racialTraits = "racial_traits"
        case url
    }
}

// MARK: - Trait
struct Trait: Codable {
    let index: String?
    let races: [Race]?
    let subraces: [SubRace]?
    let name: String?
    let desc: [String]?
    let proficiencies: [Proficiency]?
    let proficiencyChoices: [ProficiencyChoice]?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index, races, subraces, name, desc, proficiencies
        case proficiencyChoices = "proficiency_choices"
        case url
    }
}

// MARK: - Class
struct Class: Codable {
    let index, name: String?
    let hitDie: Int?
    let proficiencyChoices: [ProficiencyChoice]?
    let proficiencies, savingThrows: [Proficiency]?
    let startingEquipment: [StartingEquipment]?
    let startingEquipmentOptions: [StartingEquipmentOption]?
    let classLevels: String?
    let subclasses: [Proficiency]?
    let spellcasting: Spellcasting?
    let spells, url: String?

    enum CodingKeys: String, CodingKey {
        case index, name
        case hitDie = "hit_die"
        case proficiencyChoices = "proficiency_choices"
        case proficiencies
        case savingThrows = "saving_throws"
        case startingEquipment = "starting_equipment"
        case startingEquipmentOptions = "starting_equipment_options"
        case classLevels = "class_levels"
        case subclasses, spellcasting, spells, url
    }
}

// MARK: - ProficiencyChoice
struct ProficiencyChoice: Codable {
    let choose: Int?
    let type: String?
    let from: [Proficiency]?
}

// MARK: - Info
struct Info: Codable {
    let name: String?
    let desc: [String]?
}

// MARK: - StartingEquipmentOption
struct StartingEquipmentOption: Codable {
    let choose: Int?
    let type: String?
    let from: StartingEquipmentOptionFrom?
}

enum StartingEquipmentOptionFrom: Codable {
    case equipmentOptionFrom(EquipmentOptionFrom)
    case unionArray([FromElement])

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode([FromElement].self) {
            self = .unionArray(x)
            return
        }
        if let x = try? container.decode(EquipmentOptionFrom.self) {
            self = .equipmentOptionFrom(x)
            return
        }
        throw DecodingError.typeMismatch(StartingEquipmentOptionFrom.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for StartingEquipmentOptionFrom"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .equipmentOptionFrom(let x):
            try container.encode(x)
        case .unionArray(let x):
            try container.encode(x)
        }
    }
}

enum FromElement: Codable {
    case purpleFrom(PurpleFrom)
    case startingEquipmentArray([StartingEquipment])

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode([StartingEquipment].self) {
            self = .startingEquipmentArray(x)
            return
        }
        if let x = try? container.decode(PurpleFrom.self) {
            self = .purpleFrom(x)
            return
        }
        throw DecodingError.typeMismatch(FromElement.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for FromElement"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .purpleFrom(let x):
            try container.encode(x)
        case .startingEquipmentArray(let x):
            try container.encode(x)
        }
    }
}

// MARK: - PurpleFrom
struct PurpleFrom: Codable {
    let equipmentOption: EquipmentOption?
    let equipment: Proficiency?
    let quantity: Int?

    enum CodingKeys: String, CodingKey {
        case equipmentOption = "equipment_option"
        case equipment, quantity
    }
}

// MARK: - EquipmentOption
struct EquipmentOption: Codable {
    let choose: Int?
    let type: String?
    let from: EquipmentOptionFrom?
}

// MARK: - EquipmentOptionFrom
struct EquipmentOptionFrom: Codable {
    let equipmentCategory: Proficiency?

    enum CodingKeys: String, CodingKey {
        case equipmentCategory = "equipment_category"
    }
}

// MARK: - SubClasses
struct SubClasses: Codable {
    let count: Int?
    let results: [SubClass]?
}

// MARK: - SpellCasting
struct Spellcasting: Codable {
    let info: [Info]?
    let level: Int?
    let spellcastingAbility: SpellcastingAbility?

    enum CodingKeys: String, CodingKey {
        case info, level
        case spellcastingAbility = "spellcasting_ability"
    }
}


// MARK: - SpellcastingAbility
struct SpellcastingAbility: Codable {
    let index, name, url: String?
}

// MARK: - Level
struct Level: Codable {
    let level, abilityScoreBonuses, profBonus: Int?
    let featureChoices, features: [Class]?
    let spellcasting: [String: Int]?
    let classSpecific: [String: Int]?
    let index: String?
    let levelClass: Class?
    let url: String?
    let subclass: Class?

    enum CodingKeys: String, CodingKey {
        case level
        case abilityScoreBonuses = "ability_score_bonuses"
        case profBonus = "prof_bonus"
        case featureChoices = "feature_choices"
        case features, spellcasting
        case classSpecific = "class_specific"
        case index
        case levelClass = "class"
        case url, subclass
    }
}


// MARK: - SubClass
struct SubClass: Codable {
    let index: String?
    let subClassClass: Class?
    let name, subclassFlavor: String?
    let desc: [String]?
    let spells: [ClassSpell]?
    let subclassLevels, url: String?

    enum CodingKeys: String, CodingKey {
        case index
        case subClassClass = "class"
        case name
        case subclassFlavor = "subclass_flavor"
        case desc, spells
        case subclassLevels = "subclass_levels"
        case url
    }
}

// MARK: - ClassSpell
struct ClassSpell: Codable {
    let prerequisites: [ClassPrequisite]?
    let spell: SpellcastingAbility?
}

// MARK: - ClassPrequisite
struct ClassPrequisite: Codable {
    let index, name, url, type: String?
}

// MARK: - Feature
struct Feature: Codable {
    let index: String?
    let featureClass: Class?
    let name: String?
    let level: Int?
    let desc: [String]?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index
        case featureClass = "class"
        case name, level, desc, url
    }
}

// MARK: - StartingEquipment
struct StartingEquipment: Codable {
    let index: String?
    let startingEquipmentClass: Class?
    let startingEquipment: [StartingEquipmentElement]?
    let startingEquipmentOptions: [StartingEquipmentOption]?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index
        case startingEquipmentClass = "class"
        case startingEquipment = "starting_equipment"
        case startingEquipmentOptions = "starting_equipment_options"
        case url
    }
}

// MARK: - StartingEquipmentElement
struct StartingEquipmentElement: Codable {
    let equipment: Class?
    let quantity: Int?
}

// MARK: - Background
struct Background: Codable {
    let index, name: String?
    let startingProficiencies: [StartingProficiency]?
    let languageOptions: LanguageOptions?
    let startingEquipment: [StartingEquipment]?
    let startingEquipmentOptions: [StartingEquipmentOption]?
    let feature: Feature?
    let personalityTraits: Bonds?
    let ideals: Ideals?
    let bonds, flaws: Bonds?

    enum CodingKeys: String, CodingKey {
        case index, name
        case startingProficiencies = "starting_proficiencies"
        case languageOptions = "language_options"
        case startingEquipment = "starting_equipment"
        case startingEquipmentOptions = "starting_equipment_options"
        case feature
        case personalityTraits = "personality_traits"
        case ideals, bonds, flaws
    }
}

// MARK: - Bonds
struct Bonds: Codable {
    let from: [String]?
    let choose: Int?
    let type: String?
}

// MARK: - Ideals
struct Ideals: Codable {
    let from: [IdealsFrom]?
    let choose: Int?
    let type: String?
}

// MARK: - IdealsFrom
struct IdealsFrom: Codable {
    let desc: String?
    let alignments: [StartingProficiency]?
}

// MARK: - StartingProficiency
struct StartingProficiency: Codable {
    let index, name, url: String?
}

// MARK: - LanguageOptions
struct LanguageOptions: Codable {
    let from: [StartingProficiency]?
    let choose: Int?
    let type: String?
}

// MARK: - AbilityScore
struct AbilityScore: Codable {
    let index, name, fullName: String?
    let desc: [String]?
    let skills: [Skill]?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index, name
        case fullName = "full_name"
        case desc, skills, url
    }
}

// MARK: - Skill
struct Skill: Codable {
    let index, name: String?
    let desc: [String]?
    let abilityScore: AbilityScore?
    let url: String?

    enum CodingKeys: String, CodingKey {
        case index, name, desc
        case abilityScore = "ability_score"
        case url
    }
}

// MARK: - Proficiency
struct Proficiency: Codable {
    let index, type, name: String?
    let classes: [Subsection]?
    let races: [Race]?
    let url: String?
    let references: [ClassPrequisite]?
}
