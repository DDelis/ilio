//
//  GenericBattlefieldCell.swift
//  iLio
//
//  Created by Dimitris Delis on 5/7/21.
//

import UIKit

class GenericBattlefieldCell: BattlefieldPresentableProtocol {

    var position:Position
    var image: String? {
        didSet {
            imageView = UIImage(systemName: image ?? "")
        }
    }
    var imageView: UIImage? {
        didSet {
            if let image = imageView {
                self.delegate?.img.image = image
            } else {
                self.delegate?.img.image = nil
            }
        }
    }
    var type: FieldType = .grass {
        didSet{
            self.image = nil
            switch type {
            case .grass:
                self.delegate?.backgroundColor = .green
            case .mountain:
                self.delegate?.backgroundColor = .brown
            case .water:
                self.delegate?.backgroundColor = .blue
            case .grassWithHero:
                self.delegate?.backgroundColor = .green
                self.image = "person"
            case .grassWithMonster:
                self.delegate?.backgroundColor = .green
                self.image = "person.fill"
            }
        }
    }
    var delegate: GenericBattleCollectionViewCell? {
        didSet {
            switch self.type {
            case .grass:
                delegate?.backgroundColor = .green
            case .mountain:
                delegate?.backgroundColor = .brown
            case .water:
                delegate?.backgroundColor = .blue
            case .grassWithHero:
                delegate?.backgroundColor = .green
            case .grassWithMonster:
                delegate?.backgroundColor = .green
            }
            self.imageView = UIImage(systemName: self.image ?? "")
            delegate?.img.image = imageView
        }
    }

    func getSelected() {
        switch self.type {
        case .grass:
            self.type = .grassWithHero
        case .mountain:
            self.type = .water
        case .water:
            self.type = .grass
        case .grassWithHero:
            self.type = .grassWithMonster
        case .grassWithMonster:
            self.type = .mountain
        }
    }

    init (position: Position) {
        self.position = position
    }

}
