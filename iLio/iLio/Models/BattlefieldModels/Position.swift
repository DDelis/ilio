//
//  Position.swift
//  iLio
//
//  Created by Dimitris Delis on 5/7/21.
//

import Foundation

struct Position: Codable {

    var x: Int
    var y: Int

    var location: String {
        "\(x) \(y)"
    }
}

struct Map {

    var map:[BattlefieldPresentableProtocol] = []
    var size: Int = 0

    var monsters: Int = 0
    var heroes: Int = 0

    mutating func setUp(size: Int) {
        self.size = size
        for i in 0...size-1 {
            for j in 0...size-1 {
                map.append(GenericBattlefieldCell(position: Position(x: i, y: j)))
            }
        }
    }

    mutating func randomize() {
        var heroesGroup:Int = self.heroes
        var monstersGroup:Int = self.monsters
        self.custom()
        for var block in self.map.shuffled() {
            if block.position.x == 0 || block.position.y == 0 || block.position.x == self.size-1 || block.position.y == self.size-1 {
                block.type = .mountain
            } else {
                let rand = Int.random(in: 0...10)
                if rand < 2 {
                    block.type = .mountain
                } else if rand < 4 {
                    block.type = .water
                } else {
                    let rand = Int.random(in: 0...2)
                    if rand < 1 && heroesGroup > 0 {
                        block.type = .grassWithHero
                        heroesGroup -= 1
                    } else if rand < 2 && monstersGroup > 0 {
                        block.type = .grassWithMonster
                        monstersGroup -= 1
                    } else {
                        block.type = .grass
                    }
                }
            }
        }
    }

    mutating func custom() {
        for var block in self.map {
            if block.position.x == 0 || block.position.y == 0 || block.position.x == self.size-1 || block.position.y == self.size-1 {
                block.type = .mountain
            } else {
                block.type = .grass
            }
        }
    }

    func getCodableMap() -> CodableMap {

        var positions: [CodableGenericPosition] = []
        for block in self.map {
            positions.append(CodableGenericPosition(position: block.position, terrainType: block.type, imgUrl: block.image))
        }
        let codableMap: CodableMap = CodableMap(map: positions, id: "\(Date())", size: self.size)
        return codableMap
    }
}

struct CodableMap: Codable, Equatable {

    static func == (lhs: CodableMap, rhs: CodableMap) -> Bool {
        return lhs.id == rhs.id
    }

    var map:[CodableGenericPosition]
    var id: String
    var size: Int

    public func convertToMap() -> Map {

        var map:[BattlefieldPresentableProtocol] = []
        let size:Int = self.size
        var monsters: Int = 0
        var heroes: Int = 0

        for block in self.map {
            let position = GenericBattlefieldCell(position: block.position)
            position.type = block.terrainType
            if position.type == .grassWithMonster { monsters += 1 }
            if position.type == .grassWithHero { heroes += 1 }
            map.append(position)
        }

        let object = Map(map: map, size: size, monsters: monsters, heroes: heroes)
        return object
    }
}

struct CodableGenericPosition: Codable {

    var position: Position
    var terrainType: FieldType
    var imgUrl: String?

}

