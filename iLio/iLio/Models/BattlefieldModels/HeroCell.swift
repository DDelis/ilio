//
//  heroCell.swift
//  iLio
//
//  Created by Dimitris Delis on 5/7/21.
//

import UIKit

class HeroCell: BattlefieldPresentableProtocol {

    var image: String?

    var hero: Bool = false

    var monster: Bool = false


    var position: Position
    var heroInBlock: HeroInBlock
    var delegate: GenericBattleCollectionViewCell?
    var type: FieldType = .grass

    init(position: Position, hero: HeroInBlock) {
        self.position = position
        self.heroInBlock = hero
    }

    func getSelected() {
        
    }

}

struct HeroInBlock {

}
