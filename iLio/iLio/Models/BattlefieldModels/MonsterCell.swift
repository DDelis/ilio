//
//  monsterCell.swift
//  iLio
//
//  Created by Dimitris Delis on 5/7/21.
//

import UIKit

class MonsterCell: BattlefieldPresentableProtocol {

    var image: String?

    var hero: Bool = false

    var monster: Bool = false


    var position: Position
    var monsterInBlock: MonsterInBlock
    var delegate: GenericBattleCollectionViewCell?
    var type: FieldType = .grass

    init(position: Position, monster: MonsterInBlock) {
        self.position = position
        self.monsterInBlock = monster
    }

    func getSelected() {

    }
}

struct MonsterInBlock {

    var description: Monster

    var hitpoins:Double
    var attack:Double

    init(monster: Monster) {
        self.description = monster
        if let hitpoints = monster.hitPoints, let strength = monster.strength {
            self.hitpoins = hitpoints
            self.attack = strength
        } else {
            self.hitpoins = 0
            self.attack = 0
        }
    }


}
