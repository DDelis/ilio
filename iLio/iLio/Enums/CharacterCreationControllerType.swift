//
//  CharacterCreationControllerType.swift
//  iLio
//
//  Created by Dimitris Delis on 28/6/21.
//

import Foundation

public enum CharacterCreationControllerType {

    case general
    case NotGeneral
}

public enum StatType: Int, Codable {

    case STR
    case DEX
    case CON
    case INT
    case WIS
    case CHA

    public func getDescription() -> String {
        switch self {

        case .STR:
            return "Strength"
        case .DEX:
            return "Dexterity"
        case .CON:
            return "Constitution"
        case .INT:
            return "Intelligence"
        case .WIS:
            return "Wisdom"
        case .CHA:
            return "Charisma"
        }
    }

}
