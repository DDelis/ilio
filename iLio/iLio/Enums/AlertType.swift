//
//  AlertType.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import Foundation

/// Alert Type to feed to AlertMaker
enum AlertType {
    case EmptyPassword
    case NonValidCredentials
    case NotImplementedYet
    case ApiNotResponding
    case CharacterAlreadyExists
}

enum ConfirmAlertType {
    case confirmCharacter
    case confirmMap
    case confirmParty
    case confirmDeleteUserDefaultsAll
}



