//
//  ClassType.swift
//  iLio
//
//  Created by Dimitris Delis on 22/6/21.
//

import Foundation

enum ClassType {

    public func getIndex() -> String {
        switch self {
        case .Barbarian:
            return "barbarian"
        case .Bard:
            return "bard"
        case .Cleric:
            return "cleric"
        case .Druid:
            return "druid"
        case .Fighter:
            return "fighter"
        case .Monk:
            return "monk"
        case .Paladin:
            return "paladin"
        case .Ranger:
            return "ranger"
        case .Rogue:
            return "rogue"
        case .Sorcerer:
            return "sorcerer"
        case .Warlock:
            return "warlock"
        case .Wizard:
            return "wizard"
        }
    }

    public func getCategoryTypeForProficiencies() -> CategoryType {
        switch self {
        case .Barbarian:
            return .BarbarianProficiencies
        case .Bard:
            return .BardProficiencies
        case .Cleric:
            return .ClericProficiencies
        case .Druid:
            return .DruidProficiencies
        case .Fighter:
            return .FighterProficiencies
        case .Monk:
            return .MonkProficiencies
        case .Paladin:
            return .PaladinProficiencies
        case .Ranger:
            return .RangerProficiencies
        case .Rogue:
            return .RogueProficiencies
        case .Sorcerer:
            return .SorcererProficiencies
        case .Warlock:
            return .WarlockProficiencies
        case .Wizard:
            return .WizardProficiencies
        }
    }

    public func getCategoryTypeForSpells() -> CategoryType {
        switch self {
        case .Barbarian:
            return .BarbarianSpells
        case .Bard:
            return .BardSpells
        case .Cleric:
            return .ClericSpells
        case .Druid:
            return .DruidSpells
        case .Fighter:
            return .FighterSpells
        case .Monk:
            return .MonkSpells
        case .Paladin:
            return .PaladinSpells
        case .Ranger:
            return .RangerSpells
        case .Rogue:
            return .RogueSpells
        case .Sorcerer:
            return .SorcererSpells
        case .Warlock:
            return .WarlockSpells
        case .Wizard:
            return .WizardSpells
        }
    }

    static func getTypeFromString(_ text: String) -> ClassType? {
        if text == "Barbarian" || text == "barbarian" {
            return .Barbarian
        } else if text == "bard" || text == "Bard" {
            return .Bard
        } else if text == "Cleric" || text == "cleric" {
            return .Cleric
        } else if text == "Druid" || text == "druid" {
            return .Druid
        } else if text == "Fighter" || text == "fighter" {
            return .Fighter
        } else if text == "Monk" || text == "monk" {
            return .Monk
        } else if text == "Paladin" || text == "paladin" {
            return .Paladin
        } else if text == "Ranger" || text == "ranger" {
            return .Ranger
        } else if text == "Rogue" || text == "rogue" {
            return .Rogue
        } else if text == "Sorcerer" || text == "sorcerer" {
            return .Sorcerer
        } else if text == "Warlock" || text == "warlock" {
            return .Warlock
        } else if text == "Wizard" || text == "wizard" {
            return .Wizard
        } else {
            return nil
        }
    }

    case Barbarian
    case Bard
    case Cleric
    case Druid
    case Fighter
    case Monk
    case Paladin
    case Ranger
    case Rogue
    case Sorcerer
    case Warlock
    case Wizard
}
