//
//  FieldType.swift
//  iLio
//
//  Created by Dimitris Delis on 5/7/21.
//

import Foundation

enum FieldType: Int, Codable {
    case grass
    case mountain
    case water
    case grassWithHero
    case grassWithMonster
}
