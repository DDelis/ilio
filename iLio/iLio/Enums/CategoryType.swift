//
//  CategoryType.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import Foundation

/// Category Type for Main Category Endpoint
enum CategoryType: CategoryEndpointProtocol {


    /// get endpoint from type
    /// - Returns: can't be nil
    func getDescription() -> String {
        
        var endpoint:String = ""
        switch self {
        case .Monsters:
            endpoint = "monsters"
        case .Proficiencies:
            endpoint = "proficiencies"
        case .Backgrounds:
            endpoint = "backgrounds"
        case .Classes:
            endpoint = "classes"
        case .Races:
            endpoint = "races"
        case .Equipment:
            endpoint = "equipment-categories"
        case .Spells:
            endpoint = "spells"
        case .GameMechanics:
            endpoint = "conditions"
        case .Rules:
            endpoint = "rules"
        case .BarbarianSpells:
            endpoint = "classes/barbarian/spells"
        case .BardSpells:
            endpoint = "classes/bard/spells"
        case .ClericSpells:
            endpoint = "classes/cleric/spells"
        case .DruidSpells:
            endpoint = "classes/druid/spells"
        case .FighterSpells:
            endpoint = "classes/fighter/spells"
        case .MonkSpells:
            endpoint = "classes/monk/spells"
        case .PaladinSpells:
            endpoint = "classes/paladin/spells"
        case .RangerSpells:
            endpoint = "classes/ranger/spells"
        case .RogueSpells:
            endpoint = "classes/rogue/spells"
        case .SorcererSpells:
            endpoint = "classes/sorcerer/spells"
        case .WarlockSpells:
            endpoint = "classes/warlock/spells"
        case .WizardSpells:
            endpoint = "classes/wizard/spells"
        case .BarbarianProficiencies:
            endpoint = "classes/barbarian/proficiencies"
        case .BardProficiencies:
            endpoint = "classes/bard/proficiencies"
        case .ClericProficiencies:
            endpoint = "classes/cleric/proficiencies"
        case .DruidProficiencies:
            endpoint = "classes/druid/proficiencies"
        case .FighterProficiencies:
            endpoint = "classes/fighter/proficiencies"
        case .MonkProficiencies:
            endpoint = "classes/monk/proficiencies"
        case .PaladinProficiencies:
            endpoint = "classes/paladin/proficiencies"
        case .RangerProficiencies:
            endpoint = "classes/ranger/proficiencies"
        case .RogueProficiencies:
            endpoint = "classes/rogue/proficiencies"
        case .SorcererProficiencies:
            endpoint = "classes/sorcerer/proficiencies"
        case .WarlockProficiencies:
            endpoint = "classes/warlock/proficiencies"
        case .WizardProficiencies:
            endpoint = "classes/wizard/proficiencies"
        }
        return endpoint
    }

    case Proficiencies
    case Backgrounds
    case Classes
    case Races
    case Equipment
    case Spells
    case Monsters
    case GameMechanics
    case Rules
    case BarbarianSpells
    case BardSpells
    case ClericSpells
    case DruidSpells
    case FighterSpells
    case MonkSpells
    case PaladinSpells
    case RangerSpells
    case RogueSpells
    case SorcererSpells
    case WarlockSpells
    case WizardSpells
    case BarbarianProficiencies
    case BardProficiencies
    case ClericProficiencies
    case DruidProficiencies
    case FighterProficiencies
    case MonkProficiencies
    case PaladinProficiencies
    case RangerProficiencies
    case RogueProficiencies
    case SorcererProficiencies
    case WarlockProficiencies
    case WizardProficiencies

    public func getContentTypeFromCategory() -> ContentType {
        switch self {
        case .Backgrounds:
            return .Backgrounds
        case .Proficiencies:
            return .Proficiencies
        case .Classes:
            return .Class
        case .Equipment:
            return .Equipment
        case .GameMechanics:
            return .GameMechanics
        case .Monsters:
            return .Monster
        case .Races:
            return .Race
        case .Rules:
            return .Rules
        case .Spells:
            return .Spells
        case .BarbarianSpells:
            return .Spells
        case .BardSpells:
            return .Spells
        case .ClericSpells:
            return .Spells
        case .DruidSpells:
            return .Spells
        case .FighterSpells:
            return .Spells
        case .MonkSpells:
            return .Spells
        case .PaladinSpells:
            return .Spells
        case .RangerSpells:
            return .Spells
        case .RogueSpells:
            return .Spells
        case .SorcererSpells:
            return .Spells
        case .WarlockSpells:
            return .Spells
        case .WizardSpells:
            return .Spells
        case .BarbarianProficiencies:
            return .Proficiencies
        case .BardProficiencies:
            return .Proficiencies
        case .ClericProficiencies:
            return .Proficiencies
        case .DruidProficiencies:
            return .Proficiencies
        case .FighterProficiencies:
            return .Proficiencies
        case .MonkProficiencies:
            return .Proficiencies
        case .PaladinProficiencies:
            return .Proficiencies
        case .RangerProficiencies:
            return .Proficiencies
        case .RogueProficiencies:
            return .Proficiencies
        case .SorcererProficiencies:
            return .Proficiencies
        case .WarlockProficiencies:
            return .Proficiencies
        case .WizardProficiencies:
            return .Proficiencies
        }
    }
}
