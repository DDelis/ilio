//
//  ContentType.swift
//  iLio
//
//  Created by Dimitris Delis on 15/6/21.
//

import Foundation

/// SubCategoryType
enum ContentType: CategoryEndpointProtocol {

    /// get endpoint from type
    /// - Returns: can't be nil
    func getDescription() -> String {

        var endpoint: String = ""
        switch self {
        case .Monster:
            endpoint = "monsters"
        case .Proficiencies:
            endpoint = "proficiencies"
        case .Backgrounds:
            endpoint = "backgrounds"
        case .Class:
            endpoint = "classes"
        case .Race:
            endpoint = "races"
        case .Equipment:
            endpoint = "equipment-categories"
        case .Spells:
            endpoint = "spells"
        case .GameMechanics:
            endpoint = "conditions"
        case .Rules:
            endpoint = "rules"
        }
        return endpoint
    }

    case Monster
    case Class
    case Race
    case Proficiencies
    case Backgrounds
    case Equipment
    case Spells
    case GameMechanics
    case Rules
    
}
