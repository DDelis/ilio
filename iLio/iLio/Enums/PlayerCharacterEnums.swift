//
//  PlayerCharacterEnums.swift
//  iLio
//
//  Created by Dimitris Delis on 28/6/21.
//

import Foundation
import UIKit

enum PlayerCharacterSex: Int, Codable {

    case male
    case female

    func getDescription() -> String {
        switch self {
        case .female: return "Female"
        case .male: return "Male"
        }
    }
}

public enum CharacterDetailsType {

    case name
    case sex
    case race
    case characterClass
    case background
    case STR
    case DEX
    case CON
    case INT
    case WIS
    case CHA

    func getDescription() -> String {
        
        switch self {
        case .name:
            return "Name"
        case .sex:
            return "Sex"
        case .race:
            return "Race"
        case .characterClass:
            return "Class"
        case .background:
            return "Background"
        case .STR:
            return "Strength"
        case .DEX:
            return "Dexterity"
        case .CON:
            return "Constitution"
        case .INT:
            return "Intelligence"
        case .WIS:
            return "Wisdom"
        case .CHA:
            return "Charisma"
        }
    }

    func getGenericIcon()  -> UIImage? {
        switch self {
        case .name:
            return nil
        case .sex:
            return nil
        case .race:
            return UIImage(named: "races")
        case .characterClass:
            return UIImage(named: "classes")
        case .background:
            return UIImage(named: "backgrounds")
        case .STR:
            return UIImage(named: "Strength")
        case .DEX:
            return UIImage(named: "Dexterity")
        case .CON:
            return UIImage(named: "Constitution")
        case .INT:
            return UIImage(named: "Intelligence")
        case .WIS:
            return UIImage(named: "Wisdom")
        case .CHA:
            return UIImage(named: "Charisma")
        }
    }

}
