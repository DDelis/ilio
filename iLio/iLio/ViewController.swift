//
//  ViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import UIKit

class ViewController: StandardViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var usernameTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = ""
        self.loginButton.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        self.registerButton.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        self.usernameTextfield.delegate = self
        self.passwordTextfield.delegate = self
        self.passwordTextfield.isSecureTextEntry = true
        
        OutletAnimationManager.animateButtonAppearanceRightToCenter(self.loginButton)
        OutletAnimationManager.animateButtonAppearanceRightToCenter(self.registerButton)
        
        OutletAnimationManager.animateTextfieldAppearanceLeftToCenter(self.usernameTextfield)
        OutletAnimationManager.animateTextfieldAppearanceLeftToCenter(self.passwordTextfield)
    }
    
    
    @objc func buttonPressed(_ sender: UIButton) {
        if sender == self.loginButton {
            self.login()
        } else if sender == self.registerButton {
            self.present(AlertMaker.makeErrorAlert(.NotImplementedYet), animated: true, completion: nil)
        } else {
            print("WTF HOW ARE WE HERE")
        }
    }
    
    private func login() {

        let vc = UITabBarController()

        let vc1 = ListOfCharactersViewController()
        let nav1 = UINavigationController(rootViewController: vc1)
        nav1.tabBarItem = UITabBarItem(title: "Characters", image: UIImage(systemName: "plus"), selectedImage: UIImage(systemName: "plus"))
        vc.addChild(nav1)

        let vc2 = EncyclopediaViewController()
        let nav2 = UINavigationController(rootViewController: vc2)
        nav2.tabBarItem = UITabBarItem(tabBarSystemItem: .bookmarks, tag: 2)
        vc.addChild(nav2)

        let vc3 = MoreListViewController()
        let nav3 = UINavigationController(rootViewController: vc3)
        nav3.tabBarItem = UITabBarItem(tabBarSystemItem: .more, tag: 3)
        vc.addChild(nav3)

        //let vc = HomeViewController()
        self.navigationController?.setViewControllers([vc], animated: true)
    }

}

extension ViewController : UITextFieldDelegate {
    
    
}
