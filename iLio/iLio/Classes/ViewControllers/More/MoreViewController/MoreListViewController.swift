//
//  MoreListViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 2/7/21.
//

import UIKit

class MoreListViewController: StandardViewController {

    @IBOutlet weak var tableView: UITableView!

    private var content:[ActionProtocol] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        self.content.append(demoMoreAction(image: UIImage(named: "cart"), title: "Shop", delegate: self))
        self.content.append(AddBattlefieldAction(delegate: self))
        self.content.append(BattlefieldListAction(delegate: self))
        self.content.append(PartyManagerAction(delegate: self))
        self.content.append(ListOfPartiesAction(delegate: self))
        self.content.append(SettingsAction(delegate: self))
    }


}

struct demoMoreAction: ActionProtocol {

    var image: UIImage?

    var title: String

    var delegate: StandardViewController

    func actionPressed() {
        self.delegate.present(AlertMaker.makeErrorAlert(.NotImplementedYet), animated: true, completion: nil)
    }


}

extension MoreListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.content[indexPath.row].actionPressed()
    }
}

extension MoreListViewController: UITableViewDataSource {


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else { return UITableViewCell() }
        cell.textLabel?.text = self.content[indexPath.row].title
        cell.imageView?.image = self.content[indexPath.row].image
        return cell
    }


}
