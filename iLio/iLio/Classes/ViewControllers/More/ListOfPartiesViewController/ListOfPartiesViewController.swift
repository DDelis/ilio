//
//  ListOfPartiesViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 12/7/21.
//

import UIKit

class ListOfPartiesViewController: StandardViewController {

    @IBOutlet weak var tableView: UITableView!
    private var content: [CharacterParty] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let content = UserDefaultManager.getPartiesFromUserDefaults() {
            self.content = content
        }
    }


}

extension ListOfPartiesViewController: UITableViewDelegate {
}

extension ListOfPartiesViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else { return UITableViewCell() }
        cell.textLabel?.text = self.content[indexPath.row].id
        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if UserDefaultManager.savePlayerPartyToUserDefaults(self.content[indexPath.row], add: false) {
                self.content.remove(at: indexPath.row)
                self.tableView.reloadData()
            } else {
                self.present(AlertMaker.makeErrorAlert(.NotImplementedYet), animated: true, completion: nil)
            }
        }
    }


}

