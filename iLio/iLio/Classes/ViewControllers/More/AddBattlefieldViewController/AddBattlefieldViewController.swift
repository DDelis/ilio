//
//  AddBattlefieldViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 5/7/21.
//

import UIKit

class AddBattlefieldViewController: StandardViewController {

    @IBOutlet weak var monstersLabel: UILabel!
    @IBOutlet weak var heroesLabel: UILabel!
    @IBOutlet weak var monstersTextfield: UITextField!
    @IBOutlet weak var heroesTextfield: UITextField!
    @IBOutlet weak var randomizeButton: UIButton!
    @IBOutlet weak var customButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!

    let size: Int = 8

    var map: Map = Map()
    var codableMap: CodableMap?

    override func viewDidLoad() {
        super.viewDidLoad()

        let tapper = UITapGestureRecognizer(target: self, action: #selector(tapped))
        tapper.delegate = self
        self.view.addGestureRecognizer(tapper)

        self.navigationItem.title = "Battlefield Editor"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(save))

        self.heroesTextfield.delegate = self
        self.monstersTextfield.delegate = self
        self.heroesTextfield.placeholder = "0"
        self.monstersTextfield.placeholder = "0"
        self.monstersLabel.text = "Monters"
        self.heroesLabel.text = "Heroes"

        self.heroesTextfield.keyboardType = .numberPad
        self.monstersTextfield.keyboardType = .numberPad

        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "GenericBattleCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "GenericBattleCollectionViewCell")

        self.map.setUp(size: self.size)
        self.setUpFromExisting()
        self.collectionView.reloadData()

    }

    @objc private func save() {
        let action = UIAlertAction(title: "Confirm", style: .default) { (_) in
            if let map = self.codableMap { _ = UserDefaultManager.saveMapToUserDefaults(map, add: false) }
            if !UserDefaultManager.saveMapToUserDefaults(self.map.getCodableMap(), add: true) {
                // TODO: get to show something for this
                print("FALSE")
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
        self.present(AlertMaker.makeConfirmAlert(.confirmMap, action: action), animated: true)
    }

    private func setUpFromExisting() {
        guard let nonEmptyMap = self.codableMap else { return }
        self.map = nonEmptyMap.convertToMap()
        self.monstersTextfield.text = "\(self.map.monsters)"
        self.heroesTextfield.text = "\(self.map.heroes)"
        self.collectionView.reloadData()
    }

    @objc private func tapped() {
        self.heroesTextfield.resignFirstResponder()
        self.monstersTextfield.resignFirstResponder()
        var check: Bool = false
        if self.heroesTextfield.text != "0" && self.heroesTextfield.text != nil && self.heroesTextfield.text != "" {
            guard let heroesText = self.heroesTextfield.text, let heroes:Int = Int(heroesText) else { return }
            if self.map.heroes != heroes {
                self.map.heroes = heroes
                check = true
            }
        }
        if self.monstersTextfield.text != "0" && self.monstersTextfield.text != nil && self.monstersTextfield.text != "" {
            guard let monstersText = self.monstersTextfield.text, let monsters:Int = Int(monstersText) else { return }
            if self.map.monsters != monsters {
                self.map.monsters = monsters
                check = true
            }
        }
        if !check {
            print("No Actions to take")
        } else {
            for var block in self.map.map {
                if block.type == .grassWithHero || block.type == .grassWithMonster {
                    block.type = .grass
                }
            }
        }
        print("map heroes : \(self.map.heroes), monsters: \(self.map.monsters)")
    }

    
    @IBAction func randomizeButtonPressed(_ sender: UIButton) {
        self.map.randomize()
    }

    @IBAction func customButtonPressed(_ sender: UIButton) {
        self.map.custom()
    }
}

extension AddBattlefieldViewController: UITextFieldDelegate {

}

extension AddBattlefieldViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.map.map[indexPath.row].getSelected()
    }
}

extension AddBattlefieldViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.map.map.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GenericBattleCollectionViewCell", for: indexPath) as? GenericBattleCollectionViewCell else { return UICollectionViewCell() }
        self.map.map[indexPath.row].delegate = cell
        cell.layer.borderWidth = 1
        cell.layer.borderColor = .init(red: 0, green: 0, blue: 0, alpha: 1)
        return cell
    }


}

extension AddBattlefieldViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width/CGFloat(self.size), height: collectionView.bounds.height/CGFloat(self.size))
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension AddBattlefieldViewController: UIGestureRecognizerDelegate {

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let touch = touch.view {
            if touch.isDescendant(of: self.collectionView) {
                return false
            }
        }
        return true
    }
}
