//
//  CharacterInputTableViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 7/7/21.
//

import UIKit

class CharacterInputTableViewCell: UITableViewCell {

    @IBOutlet weak var addFromPhoneButton: UIButton!
    @IBOutlet weak var addFromQRButton: UIButton!

    weak var delegate: PartyManagerViewController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func addFromPhoneButtonPressed(_ sender: UIButton) {
        let vc = ListOfCharactersViewController()
        vc.fromPartyManager = true
        vc.delegate = self.delegate
        self.delegate?.present(vc, animated: true, completion: nil)
    }

    @IBAction func addFromQRButtonPressed(_ sender: UIButton) {
        let vc = ScannerViewController()
        vc.delegate = self.delegate
        self.delegate?.present(vc, animated: true, completion: nil)
    }

}
