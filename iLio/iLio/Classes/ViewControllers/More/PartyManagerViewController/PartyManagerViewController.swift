//
//  PartyManagerViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 7/7/21.
//

import UIKit

class PartyManagerViewController: StandardViewController {

    @IBOutlet weak var tableView: UITableView!

    private var content: [UITableViewCell] = []
    private var players: [PlayerCharacter] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Party Manager"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(saveParty))

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "CharacterInputTableViewCell", bundle: nil), forCellReuseIdentifier: "CharacterInputTableViewCell")
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        self.setUpStartingCells()
        self.tableView.reloadData()
    }

    private func setUpStartingCells() {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "CharacterInputTableViewCell") as? CharacterInputTableViewCell else { return }
        cell.delegate = self
        self.content.append(cell)
    }

    func addACharacterCell(_ player: PlayerCharacter)  {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") else { return }
        self.players.append(player)
        cell.textLabel?.text = "\(player.name ?? "") : \(player.sex ?? .male) \(player.race ?? "") \(player.characterClass ?? "")"
        _ = self.content.popLast()
        self.content.append(cell)
        self.setUpStartingCells()
        self.tableView.reloadData()
    }

    @objc private func saveParty() {
        let party = CharacterParty(id: "\(Date())", players: self.players)
        let action = UIAlertAction(title: "Confirm", style: .default) { _ in
            if !UserDefaultManager.savePlayerPartyToUserDefaults(party, add: true) {
                // false
                print("ERROR")
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }
        self.present(AlertMaker.makeConfirmAlert(.confirmParty, action: action), animated: true, completion: nil)
    }

}

extension PartyManagerViewController: UITableViewDelegate {

}

extension PartyManagerViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.content[indexPath.row]
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if self.content[indexPath.row] is CharacterInputTableViewCell { return }
            self.content.remove(at: indexPath.row)
            self.players.remove(at: indexPath.row)
            self.tableView.reloadData()
        }
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if self.content[indexPath.row] ==  self.content.last { return false }
        return true
    }

}

extension PartyManagerViewController : GetDataFromQRProtocol {

    func useData(code: String) {
        if let players = try? JSONDecoder().decode([PlayerCharacter].self, from: Data(code.utf8)) {
            self.addACharacterCell(players[0])
        }
    }
}
