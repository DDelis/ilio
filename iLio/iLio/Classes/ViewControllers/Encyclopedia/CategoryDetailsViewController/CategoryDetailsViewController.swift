//
//  CategoryDetailsViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 15/6/21.
//

import UIKit

class CategoryDetailsViewController: StandardViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var type: ContentType!
    var controller:UIAlertController?
    var contentUrl: String?
    var contentProvider: CategoryDetailsContentProvider?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib(nibName: "CategoryDetailsViewControllerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryDetailsViewControllerCollectionViewCell")
        self.collectionView.register(UINib(nibName: "CategoryDetailsMonsterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryDetailsMonsterCollectionViewCell")
        self.collectionView.register(UINib(nibName: "CategoryDetailsClassCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryDetailsClassCollectionViewCell")
        self.collectionView.register(UINib(nibName: "CategoryDetailsSpellsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryDetailsSpellsCollectionViewCell")
        self.collectionView.register(UINib(nibName: "CategoryDetailsProficienciesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryDetailsProficienciesCollectionViewCell")
        self.collectionView.register(UINib(nibName: "CategoryDetailsRaceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryDetailsRaceCollectionViewCell")
        self.collectionView.register(UINib(nibName: "CategoryDetailsEquipmentCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryDetailsEquipmentCollectionViewCell")
        self.contentProvider = CategoryDetailsContentProvider(with: self.contentUrl ?? "", delegate: self)
        self.contentProvider?.fetchContent(completionHandler: { (_, _) in
            DispatchQueue.main.async {
                if let controller = self.controller {
                    self.present(controller, animated: true, completion: nil)
                }
            }
        })
    }

}

extension CategoryDetailsViewController: UICollectionViewDelegate {

}

extension CategoryDetailsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
}

extension CategoryDetailsViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if ((self.contentProvider?.content) != nil) { return 1 } else { return 0 }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let type = self.type else { return UICollectionViewCell() }
        guard var cell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryDetailsViewControllerCollectionViewCell", for: indexPath) as? CategoryDetailsViewControllerCollectionViewCell else { return UICollectionViewCell() }

        switch type {
        case .Backgrounds:
            self.contentProvider?.getGenericCell(cell: &cell)
            return cell
        case .Proficiencies:
            guard var specificCell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryDetailsProficienciesCollectionViewCell", for: indexPath) as? CategoryDetailsProficienciesCollectionViewCell else { return UICollectionViewCell() }
            self.contentProvider?.getProficiencyContent(cell: &specificCell)
            return specificCell
        case .Class:
            guard var specificCell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryDetailsClassCollectionViewCell", for: indexPath) as? CategoryDetailsClassCollectionViewCell else { return UICollectionViewCell() }
            self.contentProvider?.getClassContent(cell: &specificCell)
            specificCell.delegate = self
            return specificCell
        case .Equipment:
            guard var specificCell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryDetailsEquipmentCollectionViewCell", for: indexPath) as? CategoryDetailsEquipmentCollectionViewCell else { return UICollectionViewCell() }
            self.contentProvider?.getEquipmentCell(&specificCell)
            return specificCell
        case .Race:
            guard var specificCell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryDetailsRaceCollectionViewCell", for: indexPath) as? CategoryDetailsRaceCollectionViewCell else { return UICollectionViewCell() }
            self.contentProvider?.getRaceContent(cell: &specificCell)
            return specificCell
        case .Spells:
            guard var specificCell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryDetailsSpellsCollectionViewCell", for: indexPath) as? CategoryDetailsSpellsCollectionViewCell else { return UICollectionViewCell() }
            self.contentProvider?.getSpellContent(cell: &specificCell)
            return specificCell
        case .GameMechanics:
            self.contentProvider?.getGenericCell(cell: &cell)
            return cell
        case .Rules:
            self.contentProvider?.getGenericCell(cell: &cell)
            return cell
        case .Monster:
            guard var specificCell = self.collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryDetailsMonsterCollectionViewCell", for: indexPath) as? CategoryDetailsMonsterCollectionViewCell else { return UICollectionViewCell() }
            self.contentProvider?.getMonsterContent(cell: &specificCell)
            return specificCell
        }
    }

}
