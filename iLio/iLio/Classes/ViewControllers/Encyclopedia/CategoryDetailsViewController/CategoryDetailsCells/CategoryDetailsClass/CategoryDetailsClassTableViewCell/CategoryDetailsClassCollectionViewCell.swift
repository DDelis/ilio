//
//  CategoryDetailsClassCollectionViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 18/6/21.
//

import UIKit

class CategoryDetailsClassCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var hitDieLabel: UILabel!
    @IBOutlet weak var subclassesLabel: UILabel!
    @IBOutlet weak var spellsUrlButton: UIButton!
    @IBOutlet weak var spellsLabel: UILabel!
    @IBOutlet weak var proficienciesUrlButton: UIButton!
    @IBOutlet weak var proficienciesLabel: UILabel!

    weak var delegate: CategoryDetailsViewController?
    var type: ClassType?
    var spellUrl: String?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.spellsLabel.text = ""
        // Initialization code
    }
    @IBAction func showSpellButtonPressed(_ sender: UIButton) {
        guard let type = self.type else { return }
        let vc = CategoryViewController()
        vc.type = type.getCategoryTypeForSpells()
        self.delegate?.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func showProficienciesButtonPressed(_ sender: UIButton) {
        guard let type = self.type else { return }
        let vc = CategoryViewController()
        vc.type = type.getCategoryTypeForProficiencies()
        self.delegate?.navigationController?.pushViewController(vc, animated: true)
    }
}
