//
//  CategoryDetailsProficienciesCollectionViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 23/6/21.
//

import UIKit

class CategoryDetailsProficienciesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var classesLabel: UILabel!
    @IBOutlet weak var racesLabel: UILabel!
    @IBOutlet weak var referencesLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
