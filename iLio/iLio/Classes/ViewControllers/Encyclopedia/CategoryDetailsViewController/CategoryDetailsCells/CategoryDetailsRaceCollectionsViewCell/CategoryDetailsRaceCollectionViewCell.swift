//
//  CategoryDetailsRaceCollectionViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 25/6/21.
//

import UIKit

class CategoryDetailsRaceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var abilityBonusLabel: UILabel!
    @IBOutlet weak var alignmentLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var weaponsLabel: UILabel!
    @IBOutlet weak var proficienciesOptionsLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var subraceLabel: UILabel!
    @IBOutlet weak var traitsLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
