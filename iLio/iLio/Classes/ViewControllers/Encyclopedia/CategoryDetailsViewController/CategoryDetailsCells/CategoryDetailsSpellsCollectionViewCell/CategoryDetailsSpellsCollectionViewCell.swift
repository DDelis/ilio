//
//  CategoryDetailsSpellsCollectionViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 22/6/21.
//

import UIKit

class CategoryDetailsSpellsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var rangeLabel: UILabel!
    @IBOutlet weak var ritualLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var concetrationLabel: UILabel!
    @IBOutlet weak var castingTimeLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var damageLabel: UILabel!
    @IBOutlet weak var dcLabel: UILabel!
    @IBOutlet weak var schoolLabel: UILabel!
    @IBOutlet weak var classesLabel: UILabel!
    @IBOutlet weak var subclassesLabel: UILabel!
    @IBOutlet weak var desc: UILabel!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
