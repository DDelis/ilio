//
//  CategoryDetailsContentProvider.swift
//  iLio
//
//  Created by Dimitris Delis on 15/6/21.
//

import Foundation
import UIKit

class CategoryDetailsContentProvider {

    var index: String
    var content:Any?
    var contentType: ContentType?
    var tableViewRows: Int = 0
    weak var delegate: CategoryDetailsViewController?

    init(with url: String, delegate: CategoryDetailsViewController) {
        self.index = ""
        self.getContentTypeAndTrimUrl(url: url)
        self.delegate = delegate
    }

    private func getContentTypeAndTrimUrl(url: String){

        print(url)
        guard url != "" else { return }
        let components = url.components(separatedBy: "/")
        guard components.count >= 3 else { return }
        if components[2] == "monsters" {
            self.contentType = .Monster
        } else if components[2] == "classes" {
            self.contentType = .Class
        } else if components[2] == "races" {
            self.contentType = .Race
        } else if components[2] == "backgrounds" {
            self.contentType = .Backgrounds
        } else if components[2] == "proficiencies" {
            self.contentType = .Proficiencies
        } else if components[2] == "equipment" {
            self.contentType = .Equipment
        } else if components[2] == "spells" {
            self.contentType = .Spells
        } else if components[2] == "classes" {
            self.contentType = .GameMechanics
        } else if components[2] == "rules" {
            self.contentType = .Rules
        }
        self.index = components[3]
    }

    func fetchContent(completionHandler: @escaping (Any?, Error?) -> ()) {
        guard let contentType = self.contentType else { return }
        NetworkManager.shared.fetchSubCategoryDetails(type: contentType, index: self.index) { (response, error) in
            if response != nil {
                switch self.contentType {
                case .Monster:
                    guard let monster = response as? Monster else { return }
                    self.content = monster
                    self.delegate?.collectionView.reloadData()
                case .Class:
                    guard let classObj = response as? Class else { return }
                    self.content = classObj
                    self.delegate?.collectionView.reloadData()
                case .Spells:
                    guard let spell = response as? Spell else { return }
                    self.content = spell
                    self.delegate?.collectionView.reloadData()
                case .Proficiencies:
                    guard let proficiency = response as? Proficiency else { return }
                    self.content = proficiency
                    self.delegate?.collectionView.reloadData()
                case .Race:
                    guard let race = response as? Race else { return }
                    self.content = race
                    self.delegate?.collectionView.reloadData()
                case .Equipment:
                    guard let equip = response as? EquipmentItem else { return }
                    self.content = equip
                    self.delegate?.collectionView.reloadData()
                default:
                    guard let subsection = response as? Subsection else { return }
                    self.content = subsection
                    self.delegate?.collectionView.reloadData()
                }
            } else  {
                self.delegate?.controller = AlertMaker.makeErrorAlert(.ApiNotResponding)
            }
        }

    }

    public func getGenericCell(cell: inout CategoryDetailsViewControllerCollectionViewCell) {
        guard let subsection = self.content as? Subsection else { return }
        cell.nameLabel.text = subsection.name
    }

    public func getMonsterContent( cell: inout CategoryDetailsMonsterCollectionViewCell) {

        guard let monster = self.content as? Monster else { return  }
        cell.nameLabel.text = "\(monster.name ?? "not defined")"
        cell.sizeLabel.text = "Size: \(monster.size ?? "not defined")"
        cell.typeLabel.text = "Type: \(monster.type ?? "not defined")"
        cell.subtypeLabel.text = "Subtype: \(monster.subtype ?? "not defined")"
        cell.alignmentLabel.text = "Alignment: \(monster.alignment ?? "not defined")"
        cell.armorClassLabel.text = "Armor: \(monster.armorClass ?? 0)"
        cell.hitpointsLabel.text = "HitPoints: \(monster.hitPoints ?? 0)"
        cell.hitDiceLabel.text = "HitDice: \(monster.hitDice ?? "not defined")"
        cell.speedLabel.text = "walk: \(monster.speed?.walk ?? "not provided") \n fly: \(monster.speed?.fly ?? "not provided") \n swim: \(monster.speed?.swim ?? "not provided")"
        cell.strengthLabel.text = "Str: \(monster.strength ?? 0)"
        cell.dexterityLabel.text = "Dext: \(monster.dexterity ?? 0)"
        cell.constitutionLabel.text = "Const: \(monster.constitution ?? 0)"
        cell.intelligenceLabel.text = "Int: \(monster.intelligence ?? 0)"
        cell.wisdomLabel.text = "Wisdom: \(monster.wisdom ?? 0)"
        cell.charismaLabel.text = "Char: \(monster.charisma ?? 0)"

    }

    public func getSpellContent( cell: inout CategoryDetailsSpellsCollectionViewCell) {
        guard let spell = self.content as? Spell,
              let ritual = spell.ritual,
              let concetration = spell.concentration,
              let classes = spell.classes,
              let subclasses = spell.subclasses,
              let desc = spell.desc
        else { return }
        cell.rangeLabel.text = "Range: \(spell.range ?? "Not defined")"
        cell.ritualLabel.text = "Ritual: " + (ritual ? "Required" : "Not Required")
        cell.durationLabel.text = "Duration: \(spell.duration ?? "not defined")"
        cell.concetrationLabel.text = "Concetration: " + (concetration ? "Required" : "Not Required")
        cell.castingTimeLabel.text = "Casting Time: \(spell.castingTime ?? "not defined")"
        cell.levelLabel.text = "Level Required: \(spell.level ?? 0)"
        var damageText = "Dmg Type: \(spell.damage?.damageType?.name ?? "not defined")"
        if let damageAtCharacterLevel = spell.damage?.damageAtCharacterLevel {
            for (key,value) in damageAtCharacterLevel {
                damageText = "\(damageText)\n level\(key) : \(value)"
            }
        }
        cell.damageLabel.text = damageText
        cell.dcLabel.text = "Dc Type: \(spell.dc?.dcType?.name ?? "not defined")"
        cell.schoolLabel.text = "School of Spells : \(spell.school?.name ?? "not defined")"
        var classesText = "Classes:"
        if !classes.isEmpty {
            for clas in classes {
                classesText = "\(classesText) \(clas.name ?? "")"
            }
        } else { classesText = "Not defined classes" }
        cell.classesLabel.text = classesText
        var subclassesText = "Subclasses:"
        if !subclasses.isEmpty {
            for subclas in subclasses {
                subclassesText = "\(subclassesText) \(subclas.name ?? "")"
            }
        } else { subclassesText = "Not defined subclasses" }
        cell.subclassesLabel.text = subclassesText
        var descText = ""
        if !desc.isEmpty {
            descText = desc[0]
        }
        cell.desc.text = descText

    }

    public func getClassContent(cell: inout CategoryDetailsClassCollectionViewCell) {

        guard let classObj = self.content as? Class else { return }
        cell.hitDieLabel.text = "hitDie: \(classObj.hitDie ?? 0)"
        var subclassesText = "Subclasses: "
        if let subclasses = classObj.subclasses, !subclasses.isEmpty {
            for subclass in subclasses {
                subclassesText = "\(subclassesText)\(subclass.name ?? "")\n"
            }
        } else {
            subclassesText = "\(subclassesText)No subclasses"
        }
        if let type = ClassType.getTypeFromString(classObj.name ?? "") {
            cell.type = type
        }
        cell.subclassesLabel.text = subclassesText
        cell.spellUrl = classObj.spells
    }


    public func getProficiencyContent(cell: inout CategoryDetailsProficienciesCollectionViewCell) {

        guard let proficiency = self.content as? Proficiency else { return }
        cell.typeLabel.text = "Type: \(proficiency.type ?? "not defined")"
        var classesText = "Classes: "
        if let classes = proficiency.classes, !classes.isEmpty {
            for clas in classes {
                classesText = "\(classesText) \(clas.name ?? "")"
            }
        } else {
            classesText = "\(classesText) not defined"
        }
        cell.classesLabel.text = classesText
        var racesText = "Races: "
        if let races = proficiency.races, !races.isEmpty {
            for race in races {
                racesText = "\(racesText) \(race.name ?? "")"
            }
        } else {
            racesText = "\(racesText) not defined"
        }
        cell.racesLabel.text = racesText
        var refText = "References: "
        if let refs = proficiency.references, !refs.isEmpty {
            for ref in refs {
                refText = "\(refText) \(ref.name ?? "")"
            }
        } else {
            refText = "\(refText) not defined"
        }
        cell.referencesLabel.text = refText

    }

    public func getRaceContent(cell: inout CategoryDetailsRaceCollectionViewCell) {

        guard let race = self.content as? Race else { return }
        cell.speedLabel.text = "Speed: \(race.speed ?? 0)"
        var abilityBonusText = "Ability Bonuses: "
        if let bonuses = race.abilityBonuses {
            for bonus in bonuses {
                abilityBonusText = "\(abilityBonusText) \(bonus.abilityScore?.name ?? "") : \(bonus.bonus  ?? 0) \n"
            }
        } else { abilityBonusText = "\(abilityBonusText) not defined" }
        cell.abilityBonusLabel.text = abilityBonusText
        cell.alignmentLabel.text = "Alignment: \(race.alignment ?? "not defined")"
        cell.sizeLabel.text = "Size: (\(race.size ?? "not defined")) \(race.sizeDescription ?? "No description provided")"
        var weaponProficienciesText:String = "Weapon Proficiencies: "
        var count:Int = 0
        if let weapons = race.startingProficiencies {
            for weapon in weapons {
                count += 1
                if count % 2 == 0 {
                    weaponProficienciesText = "\(weaponProficienciesText) \n"
                }
                weaponProficienciesText = "\(weaponProficienciesText) \(weapon.name ?? "not defined")"
            }
        } else { weaponProficienciesText = "\(weaponProficienciesText) not defined"}
        cell.weaponsLabel.text = weaponProficienciesText
        var proficiencyOptionText:String = "Proficiency Options (Can choose \(race.startingProficiencyOptions?.choose ?? 0)): "
        count = 0
        if let startingProficiencies = race.startingProficiencyOptions?.from {
            for proficiency in startingProficiencies {
                count += 1
                if count % 2 == 0 {
                    proficiencyOptionText = "\(proficiencyOptionText) \n"
                }
                proficiencyOptionText = "\(proficiencyOptionText) \(proficiency.name ?? "not defined")"
            }
        } else { proficiencyOptionText = "\(proficiencyOptionText) not defined"}
        cell.proficienciesOptionsLabel.text = proficiencyOptionText
        var languagesText:String = "Languages: "
        if let languages = race.languages {
            for lang in languages {
                languagesText = "\(languagesText) \(lang.name ?? "not defined")"
            }
        } else { languagesText = "\(languagesText) not defined"}
        cell.languageLabel.text = "\(languagesText)  : \(race.languageDesc ?? "description not provided")"
        var traitsText:String = "Traits: "
        count = 0
        if let traits = race.traits {
            for trait in traits {
                count += 1
                if count % 2 == 0 {
                    traitsText = "\(traitsText) \n"
                }
                traitsText = "\(traitsText) \(trait.name ?? "not defined")"
            }
        } else { traitsText = "\(traitsText) not defined"}
        cell.traitsLabel.text = traitsText
        var subracesText:String = "Subraces: "
        if let subraces = race.subraces {
            for subrace in subraces {
                subracesText = "\(subracesText) \(subrace.name ?? "not defined")"
            }
        } else { subracesText = "\(subracesText) not defined"}
        cell.subraceLabel.text = subracesText

    }

    public func getEquipmentCell(_ cell: inout CategoryDetailsEquipmentCollectionViewCell) {
        
    }
}
