//
//  CategoryViewControllerCollectionViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import UIKit

class CategoryViewControllerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var action: ActionProtocol? {
        didSet{
            self.titleLabel.text = self.action?.title
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
