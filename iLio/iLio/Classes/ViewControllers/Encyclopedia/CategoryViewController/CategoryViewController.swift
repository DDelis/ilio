//
//  CategoryViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import UIKit

class CategoryViewController: StandardViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var actions:[ActionProtocol] = []
    var type: CategoryType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "CategoryViewControllerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryViewControllerCollectionViewCell")
        
        guard let type = self.type else { return }
        NetworkManager.shared.fetchCategories(type: type) { (response, error) in
            if (error != nil) {
                DispatchQueue.main.async {
                    self.present(AlertMaker.makeErrorAlert(.ApiNotResponding), animated: true, completion: nil)
                }
            } else if response != nil {
                if let response = response as? Category  {
                    let subCategories = response.results
                    for subCategory in subCategories {
                        self.actions.append(SubCategoryAction(image: nil, title: subCategory.name, type: self.type?.getContentTypeFromCategory() ?? .Backgrounds, delegate: self, url: subCategory.url ?? ""))
                    }
                    self.collectionView.reloadData()
                }
                
            } else {
                print("JSON ERROR")
            }
        }

    }


}

extension CategoryViewController: UICollectionViewDelegate {}

extension CategoryViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: UIScreen.main.bounds.width, height: 35)
    }
}

extension CategoryViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.actions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryViewControllerCollectionViewCell", for: indexPath) as? CategoryViewControllerCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.action = self.actions[indexPath.row]
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.actions[indexPath.row].actionPressed()
    }
    
}
