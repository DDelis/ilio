//
//  EncyclopediaViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import UIKit

class EncyclopediaViewController: StandardViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private var actions:[ActionProtocol] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "HomeViewControllerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeViewControllerCollectionViewCell")

        self.actions.append(EncyclopediaCategoryAction(image: UIImage(named: "proficiencies")!, title: "proficiencies", delegate: self, type: .Proficiencies))
        self.actions.append(EncyclopediaCategoryAction(image: UIImage(named: "backgrounds")!, title: "backgrounds", delegate: self, type: .Backgrounds))
        self.actions.append(EncyclopediaCategoryAction(image: UIImage(named: "classes")!, title: "classes", delegate: self, type: .Classes))
        self.actions.append(EncyclopediaCategoryAction(image: UIImage(named: "races")!, title: "races", delegate: self, type: .Races))
        self.actions.append(EncyclopediaCategoryAction(image: UIImage(named: "equipment")!, title: "equipment", delegate: self, type: .Equipment))
        self.actions.append(EncyclopediaCategoryAction(image: UIImage(named: "spells")!, title: "spells", delegate: self, type: .Spells))
        self.actions.append(EncyclopediaCategoryAction(image: UIImage(named: "monsters")!, title: "monsters", delegate: self, type: .Monsters))
        self.actions.append(EncyclopediaCategoryAction(image: UIImage(named: "game mechanics")!, title: "game mechanics", delegate: self, type: .GameMechanics))
        self.actions.append(EncyclopediaCategoryAction(image: UIImage(named: "rules")!, title: "rules", delegate: self, type: .Rules))
    }

}

extension EncyclopediaViewController: UICollectionViewDelegate {}

extension EncyclopediaViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (UIScreen.main.bounds.width/3)-10, height: 128)
    }
}

extension EncyclopediaViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.actions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeViewControllerCollectionViewCell", for: indexPath) as? HomeViewControllerCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.action = self.actions[indexPath.row]
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.actions[indexPath.row].actionPressed()
    }
    
}
