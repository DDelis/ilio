//
//  HomeViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import UIKit

/// Not Used Currently
class HomeViewController: StandardViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var actions:[ActionProtocol] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "HomeViewControllerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "HomeViewControllerCollectionViewCell")
        
        self.actions.append(AddAction(delegate: self))
        self.actions.append(ViewEncyclopediaAction(delegate: self))
        self.actions.append(DemoAction(image: UIImage(systemName: "rectangle.split.3x1")!, title: "rectangle.split.3x1", delegate: self))
        self.actions.append(DemoAction(image: UIImage(systemName: "arrow.up.left.and.arrow.down.right")!, title: "arrow.up.left.and.arrow.down.right", delegate: self))
        self.actions.append(DemoAction(image: UIImage(systemName: "lock.open")!, title: "lock.open", delegate: self))
        self.actions.append(DemoAction(image: UIImage(systemName: "eye")!, title: "eye", delegate: self))

    }


}

extension HomeViewController: UICollectionViewDelegate {}

extension HomeViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (UIScreen.main.bounds.width/3)-10, height: 128)
    }
}

extension HomeViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.actions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeViewControllerCollectionViewCell", for: indexPath) as? HomeViewControllerCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.action = self.actions[indexPath.row]
        cell.layer.borderColor = UIColor.black.cgColor
        cell.layer.borderWidth = 1
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.actions[indexPath.row].actionPressed()
    }
    
}
