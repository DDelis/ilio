//
//  HomeViewControllerCollectionViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import UIKit

class HomeViewControllerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var action:ActionProtocol? {
        didSet{
            self.imageView.image = self.action?.image
            self.titleLabel.text = self.action?.title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
