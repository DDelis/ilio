//
//  CharacterDetailsViewControllerCollectionViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 29/6/21.
//

import UIKit

class CharacterDetailsViewControllerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var TitleButton: UIButton!
    var type: CharacterDetailsType = .name {
        didSet {
            self.img.image = type.getGenericIcon()
            if type == .name || type == .sex {
                self.img.isHidden = true
            }
            self.TitleButton.setTitle(type.getDescription(), for: .normal)
        }
    }
    var url: String?
    override func awakeFromNib() {

        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func buttonPressed(_ sender: UIButton) {

    }
}
