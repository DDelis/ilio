//
//  CharacterDetailsViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 29/6/21.
//

import UIKit

class CharacterDetailsViewController: StandardViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var character: PlayerCharacter?
    var content:[CharacterDetailsType] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = self.character?.name

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(getPlayerQRCode))

        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.register(UINib(nibName: "CharacterDetailsViewControllerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CharacterDetailsViewControllerCollectionViewCell")
        self.setUpContent()


    }

    @objc private func getPlayerQRCode() {
        let vc = QRViewController()
        guard let img = self.character?.getQRImage() else { return }
        vc.image = img
        self.present(vc, animated: true, completion: nil)
    }

    private func setUpContent() {

        self.content.append(.name)
        self.content.append(.sex)
        self.content.append(.race)
        self.content.append(.characterClass)
        self.content.append(.background)
        self.content.append(.STR)
        self.content.append(.DEX)
        self.content.append(.CON)
        self.content.append(.INT)
        self.content.append(.WIS)
        self.content.append(.CHA)

    }

    private func setUpCell(cell: inout CharacterDetailsViewControllerCollectionViewCell, type: CharacterDetailsType) {
        cell.type = type
        switch type {
        case .name:
            cell.valueLabel.text = self.character?.name
        case .sex:
            cell.valueLabel.text = self.character?.sex?.getDescription()
        case .race:
            cell.valueLabel.text = self.character?.race
        case .characterClass:
            cell.valueLabel.text = self.character?.characterClass
        case .background:
            cell.valueLabel.text = self.character?.background
        case .STR:
            cell.valueLabel.text = "\(self.character?.stats.STR ?? 0)"
        case .DEX:
            cell.valueLabel.text = "\(self.character?.stats.DEX ?? 0)"
        case .CON:
            cell.valueLabel.text = "\(self.character?.stats.CON ?? 0)"
        case .INT:
            cell.valueLabel.text = "\(self.character?.stats.INT ?? 0)"
        case .WIS:
            cell.valueLabel.text = "\(self.character?.stats.WIS ?? 0)"
        case .CHA:
            cell.valueLabel.text = "\(self.character?.stats.CHA ?? 0)"
        }
    }

}

extension CharacterDetailsViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch content[indexPath.row] {
        case .name:
            return CGSize(width: UIScreen.main.bounds.width, height: 35)
        case .sex:
            return CGSize(width: UIScreen.main.bounds.width, height: 35)
        default:
            return CGSize(width: UIScreen.main.bounds.width, height: 64)
        }
    }
}

extension CharacterDetailsViewController: UICollectionViewDelegate {

}

extension CharacterDetailsViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return content.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard var cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CharacterDetailsViewControllerCollectionViewCell", for: indexPath) as? CharacterDetailsViewControllerCollectionViewCell else { return UICollectionViewCell() }
        self.setUpCell(cell: &cell, type: self.content[indexPath.row])
        return cell
    }

}
