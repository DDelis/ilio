//
//  ListOfCharactersViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 28/6/21.
//

import UIKit

class ListOfCharactersViewController: StandardViewController {

    @IBOutlet weak var tableView: UITableView!
    private var characters:[PlayerCharacter] = []

    public var fromPartyManager: Bool = false
    weak var delegate: PartyManagerViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        self.navigationItem.title = "Characters"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addCharacter))

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.characters = []
        if let characters = UserDefaultManager.getCharacterFromUserDefaults() {
            self.characters = characters
            self.tableView.reloadData()
        } else {
            self.tableView.reloadData()
        }
    }

    @objc func addCharacter() {
        let vc = CharacterCreationViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

extension ListOfCharactersViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !self.fromPartyManager {
            let vc = CharacterDetailsViewController()
            vc.character = self.characters[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.delegate?.addACharacterCell(self.characters[indexPath.row])
            self.dismiss(animated: true, completion: nil)
        }
    }

}

extension ListOfCharactersViewController : UITableViewDataSource {


    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.characters.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else { return UITableViewCell() }
        cell.textLabel?.text = "\(self.characters[indexPath.row].name ?? "") : \(self.characters[indexPath.row].sex ?? .male) \(self.characters[indexPath.row].race ?? "") \(self.characters[indexPath.row].characterClass ?? "")"
        return cell
    }

    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Are you sure you wanna delete \(self.characters[indexPath.row].name ?? "")?"
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if UserDefaultManager.saveCharacterToUserDefaults(self.characters[indexPath.row], add: false) {
                self.characters.remove(at: indexPath.row)
                self.tableView.reloadData()
            } else {
                self.present(AlertMaker.makeErrorAlert(.NotImplementedYet), animated: true, completion: nil)
            }
        }
    }


}
