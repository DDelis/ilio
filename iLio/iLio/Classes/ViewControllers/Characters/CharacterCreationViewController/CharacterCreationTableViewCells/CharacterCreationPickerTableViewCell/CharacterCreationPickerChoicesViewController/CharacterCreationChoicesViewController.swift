//
//  CharacterCreationChoicesViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 29/6/21.
//

import UIKit

class CharacterCreationChoicesViewController: StandardViewController {

    @IBOutlet weak var tableView: UITableView!
    var content: [String] = []
    weak var delegate: CharacterCreationPickerTableViewCell?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        // Do any additional setup after loading the view.
    }

}

extension CharacterCreationChoicesViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.value = self.content[indexPath.row]
        self.dismiss(animated: true, completion: nil)
    }

}

extension CharacterCreationChoicesViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") else { return UITableViewCell() }
        cell.textLabel?.text = content[indexPath.row]
        return cell
    }


}
