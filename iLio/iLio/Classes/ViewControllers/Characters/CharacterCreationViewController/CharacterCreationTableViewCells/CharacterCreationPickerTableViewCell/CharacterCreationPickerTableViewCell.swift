//
//  CharacterCreationPickerTableViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 28/6/21.
//

import UIKit

class CharacterCreationPickerTableViewCell:  UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var imageViewObject: UIImageView!
    var check: Bool = false
    weak var delegate: CreationWithPickersProtocol?
    var value: String? {
        didSet{
            self.valueLabel.text = value
            self.delegate?.tapped()
        }
    }

    private var index:Int = 0

    var pickerContent: [String]? {
        didSet {
            check = true
            self.imageView?.isHidden = false

        }
    }
    var title: String? {
        didSet {
            self.titleLabel.text = "\(title ?? "") :"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageView?.isHidden = true
        self.valueLabel.text = "Not Selected"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func pressed() {
        if check {
            let vc = CharacterCreationChoicesViewController()
            vc.content = self.pickerContent!
            vc.delegate = self
            self.delegate?.present(vc, animated: true, completion: nil)
        }
    }
    
}

extension CharacterCreationPickerTableViewCell: CharacterCreationCellProtocol {

    func hideKeyboard() {
        
    }

    func getValue() -> String? {
        return self.value
    }
    
}
