//
//  StatTableViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 29/6/21.
//

import UIKit

class StatTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueTextfield: UITextField!
    @IBOutlet weak var randomButton: UIButton!

    weak var delegate: CharacterCreationViewController?
    var type: StatType? {
        didSet {
            titleLabel.text = self.type?.getDescription()
        }
    }
    var value: Int? {
        didSet {
            self.valueTextfield.text = "\(value ?? 0)"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.valueTextfield.delegate = self
        self.valueTextfield.keyboardType = .numberPad
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func randomPressed(_ sender: UIButton) {
        self.value = Int.random(in: 0...12)
        self.delegate?.tapped()
    }

}

extension StatTableViewCell: CharacterCreationCellProtocol {


    func getValue() -> String? {
        if value != nil {
            return "\(value ?? 0)"
        } else {
            return nil
        }
    }

    func hideKeyboard() {
        self.valueTextfield.resignFirstResponder()
    }

}

extension StatTableViewCell: UITextFieldDelegate {

    func textFieldDidChangeSelection(_ textField: UITextField) {
        self.value = Int(textField.text ?? "")
    }

}
