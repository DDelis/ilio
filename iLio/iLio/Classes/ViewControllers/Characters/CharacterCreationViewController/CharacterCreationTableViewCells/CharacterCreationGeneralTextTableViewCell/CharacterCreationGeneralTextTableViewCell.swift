//
//  CharacterCreationGeneralTextTableViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 28/6/21.
//

import UIKit

class CharacterCreationGeneralTextTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inputTextfield: UITextField!

    var title: String? {
        didSet {
            self.titleLabel.text = "\(title ?? "") :"
            self.inputTextfield.placeholder = title
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.inputTextfield.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

extension CharacterCreationGeneralTextTableViewCell: UITextFieldDelegate {

}

extension CharacterCreationGeneralTextTableViewCell: CharacterCreationCellProtocol {

    func hideKeyboard() {
        self.inputTextfield.resignFirstResponder()
    }

    func getValue() -> String? {
        if (inputTextfield.text == nil || inputTextfield.text == "" || inputTextfield.text == "name") {
            return nil
        }
        return inputTextfield.text
    }

}
