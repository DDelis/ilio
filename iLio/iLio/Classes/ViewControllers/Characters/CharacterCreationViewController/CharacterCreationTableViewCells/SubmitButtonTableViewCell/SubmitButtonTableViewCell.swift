//
//  SubmitButtonTableViewCell.swift
//  iLio
//
//  Created by Dimitris Delis on 29/6/21.
//

import Foundation
import UIKit

class SubmitButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var submitButton: UIButton!
    weak var delegate: CharacterCreationViewController?
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func buttonPressed(_ sender: UIButton) {
        OutletAnimationManager.animateButtonPressed(sender) {
            self.delegate?.submit()
        }
    }

}

extension SubmitButtonTableViewCell: CharacterCreationCellProtocol {

    func hideKeyboard() {
        
    }

    func getValue() -> String? {
        return nil
    }
}
