//
//  CharacterCreationViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 28/6/21.
//

import UIKit
import Lottie

class CharacterCreationViewController: StandardViewController, CreationWithPickersProtocol {

    @IBOutlet weak var tableView: UITableView!
    var character: PlayerCharacter?
    var content:[UITableViewCell & CharacterCreationCellProtocol] = []
    var type: CharacterCreationControllerType = .general

    override func viewDidLoad() {
        super.viewDidLoad()

        let tapper = UITapGestureRecognizer(target: self, action: #selector(tapped))
        tapper.delegate = self
        self.view.addGestureRecognizer(tapper)

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "CharacterCreationGeneralTextTableViewCell", bundle: nil), forCellReuseIdentifier: "CharacterCreationGeneralTextTableViewCell")
        self.tableView.register(UINib(nibName: "CharacterCreationPickerTableViewCell", bundle: nil), forCellReuseIdentifier: "CharacterCreationPickerTableViewCell")
        self.tableView.register(UINib(nibName: "SubmitButtonTableViewCell", bundle: nil), forCellReuseIdentifier: "SubmitButtonTableViewCell")
        self.tableView.register(UINib(nibName: "StatTableViewCell", bundle: nil), forCellReuseIdentifier: "StatTableViewCell")
        self.setUpCells(self.type)
    }

    private func setUpCells(_ type: CharacterCreationControllerType) {
        switch type {
        case .general:
            self.content.append(self.setUpCharacterCreationGeneralTextTableViewCell("name")!)
            self.setUpCharacterCreationPickerTableViewCell("Race", type: .Races)
            self.setUpCharacterCreationPickerTableViewCell("Class", type: .Classes)
        case .NotGeneral:
            self.setUpStatsCells()
            break
        }
        self.tableView.reloadData()
    }

    private func setUpCharacterCreationGeneralTextTableViewCell(_ text: String) -> CharacterCreationGeneralTextTableViewCell? {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "CharacterCreationGeneralTextTableViewCell") as? CharacterCreationGeneralTextTableViewCell else { return nil
        }
        cell.title = text
        return cell
    }

    private func setUpStatsCells() {
        self.content.append(self.createStatCell(type: .STR))
        self.content.append(self.createStatCell(type: .DEX))
        self.content.append(self.createStatCell(type: .CON))
        self.content.append(self.createStatCell(type: .INT))
        self.content.append(self.createStatCell(type: .CHA))
        self.content.append(self.createStatCell(type: .WIS))
    }

    private func createStatCell(type: StatType) -> StatTableViewCell {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "StatTableViewCell") as? StatTableViewCell else { return StatTableViewCell() }
        cell.delegate = self
        cell.type = type
        return cell
    }

    private func setUpCharacterCreationPickerTableViewCell(_ text: String, type: CategoryType) {
        NetworkManager.shared.fetchCategories(type: type) { (response, error) in
            if error != nil {
                self.present(AlertMaker.makeErrorAlert(.ApiNotResponding), animated: true)
            } else {
                var array: [String] = []
                if let names = response as? Category {
                    for name in names.results {
                        array.append(name.name)
                    }
                }
                guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "CharacterCreationPickerTableViewCell") as? CharacterCreationPickerTableViewCell else { return }
                cell.pickerContent = array
                cell.title = text
                cell.delegate = self
                self.content.append(cell)
                self.tableView.reloadData()
            }
        }
    }

    private func setUpSubmitButtonCell() {
        guard let cell = self.tableView.dequeueReusableCell(withIdentifier: "SubmitButtonTableViewCell") as? SubmitButtonTableViewCell else { return }
        cell.delegate = self
        self.content.append(cell)
        self.tableView.reloadData()
    }

    @objc func tapped() {
        var check = true
        for cell in self.content {
            cell.hideKeyboard()
            if !(cell is SubmitButtonTableViewCell) && (cell.getValue() == nil) {
                check = false
            }
        }
        if check && !self.content.contains(where: {$0 is SubmitButtonTableViewCell}) {
            self.setUpSubmitButtonCell()
            print("Adding submit button cell")
        }
        print("not doing anything")
    }

    func submit() {
        switch self.type {
        case .general:
            let vc = CharacterCreationViewController()
            vc.type = .NotGeneral
            self.character = PlayerCharacter(name: self.content[0].getValue(), sex: .male, race: self.content[1].getValue(), characterClass: self.content[2].getValue(), level: nil, background: nil, stats: Stats(random: true))
            vc.character = self.character
            self.navigationController?.pushViewController(vc, animated: true)
        case .NotGeneral:
            let str = Int(self.content[0].getValue()!)
            let dex = Int(self.content[1].getValue()!)
            let con = Int(self.content[2].getValue()!)
            let int = Int(self.content[3].getValue()!)
            let cha = Int(self.content[4].getValue()!)
            let wis = Int(self.content[5].getValue()!)
            let stat = Stats(STR: str!, DEX: dex!, CON: con!, INT: int!, CHA: cha!, WIS: wis!)
            self.character?.stats = stat
            let action = UIAlertAction(title: "Confirm", style: .default) { (_) in
                if !UserDefaultManager.saveCharacterToUserDefaults(self.character!, add: true) {
                    // TODO: get to show something for this
                    print("FALSE")
                } else {
                    let vc = AnimationPresentationViewController()
                    let animation = Animation.named("characterJumpingAnimation")
                    vc.presentAnimationAndTitle(animation, _title: "Congratulations")
                    self.present(vc, animated: true, completion: nil)
                    for controller in self.navigationController!.viewControllers {
                        if controller is ListOfCharactersViewController {
                            self.navigationController?.popToViewController(controller, animated: true)
                        }
                    }
                }
            }
            self.present(AlertMaker.makeConfirmAlert(.confirmCharacter, action: action), animated: true)
        }
    }


}

extension CharacterCreationViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath) is CharacterCreationPickerTableViewCell {
            (tableView.cellForRow(at: indexPath) as! CharacterCreationPickerTableViewCell).pressed()
        }
        tableView.deselectRow(at: indexPath, animated: true)

    }
}

extension CharacterCreationViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return content[indexPath.row]
    }


}

extension CharacterCreationViewController: UIGestureRecognizerDelegate {

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let touch = touch.view {
            for cell in self.content {
                if touch.isDescendant(of: cell) {
                    return false
                }
            }
        }
        return true
    }
}
