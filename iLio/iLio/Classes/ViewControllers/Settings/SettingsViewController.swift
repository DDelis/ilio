//
//  SettingsViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 30/6/21.
//

import UIKit

class SettingsViewController: StandardViewController {

    @IBOutlet weak var tableView: UITableView!

    private var content: [SettingsActionProtocol] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Settings"

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")

        self.content.append(ResetDataAction(delegate: self))
    }

}

extension SettingsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.content[indexPath.row].actionPressed()
    }
}

extension SettingsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") else { return UITableViewCell() }
        cell.textLabel?.text = self.content[indexPath.row].title
        cell.imageView?.image = self.content[indexPath.row].image
        return cell
    }
}
