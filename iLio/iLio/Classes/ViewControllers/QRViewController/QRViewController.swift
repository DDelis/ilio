//
//  QRViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 7/7/21.
//

import UIKit

class QRViewController: StandardViewController {

    @IBOutlet weak var img: UIImageView!
    var image: UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let image = self.image else { return }
        self.img.image = image

        // Do any additional setup after loading the view.
    }

}
