//
//  AnimationPresentationViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 2/7/21.
//

import UIKit
import Lottie

class AnimationPresentationViewController: StandardViewController {

    @IBOutlet weak var titleLabel: UILabel!
    var titleLabelText: String = ""
    var animationView: AnimationView = AnimationView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.animationView.frame = CGRect(x: 0, y: 150, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height-150)
        self.view.addSubview(animationView)
        self.modalPresentationStyle = .fullScreen
        self.titleLabel.text = title

        self.animationPlay()
    }


    private func animationPlay() {
        self.animationView.play { (_) in
            self.dismiss(animated: true, completion: nil)
        }
    }

    public func presentAnimationAndTitle(_ animation: Animation?, _title: String) {
        self.titleLabelText = _title
        self.animationView.animation = animation
    }


}
