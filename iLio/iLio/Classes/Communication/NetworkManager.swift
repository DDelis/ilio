//
//  NetworkManager.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import Foundation
import Alamofire

class NetworkManager: NetworkManagerProtocol {
    
    static let shared = NetworkManager(baseURL: URL(string: "https://www.dnd5eapi.co/api/")!)
    
    let baseURL: URL
    private init(baseURL: URL) {
        self.baseURL = baseURL
    }

    private func getEndpointFromType(type: CategoryEndpointProtocol) -> String {
        return type.getDescription()
    }
    
    func fetchCategories(type: CategoryType, completion: @escaping (Any?, Error?)-> ()) {
        
        let endpoint:String = self.getEndpointFromType(type: type)
        
        guard let url = URL(string: "\(baseURL)\(endpoint)") else {
            completion (nil, nil)
            return
        }
        
        AF.request(url, method: .get).response { response in
            
            switch response.result {
            case .success(let data):
                let decoder = JSONDecoder()
                guard let data = data else { return }
                do {
                    let categories = try decoder.decode(Category.self, from: data)
                    completion(categories, nil)
                } catch let DecodingError.dataCorrupted(context) {
                    print(context)
                    completion(nil, nil)
                } catch let DecodingError.keyNotFound(key, context) {
                    print("Key '\(key)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                    completion(nil, nil)
                } catch let DecodingError.valueNotFound(value, context) {
                    print("Value '\(value)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                    completion(nil, nil)
                } catch let DecodingError.typeMismatch(type, context)  {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                    completion(nil, nil)
                } catch {
                    print("error: ", error)
                    completion(nil, nil)
                }
            case .failure(let error):
                completion(nil, error)
            }
            
        }
        
    }

    func fetchSubCategoryDetails(type: ContentType, index: String, completion: @escaping (Any?, Error?)-> ()) {

        var endpoint:String = self.getEndpointFromType(type: type)
        
        endpoint = "\(endpoint)/\(index)"
        guard let url = URL(string: "\(baseURL)\(endpoint)") else {
            completion(nil, nil)
            return
        }

        AF.request(url, method: .get).response { response in
            switch response.result {
            case .success(let data):
                let decoder = JSONDecoder()
                guard let data = data else { return }
                do {
                    var subcategory: Any?
                    switch type {
                    case .Monster:
                        subcategory = try decoder.decode(Monster.self, from: data)
                    case .Class:
                        subcategory = try decoder.decode(Class.self, from: data)
                    case .Spells:
                        subcategory = try decoder.decode(Spell.self, from: data)
                    case .Proficiencies:
                        subcategory = try decoder.decode(Proficiency.self, from: data)
                    case .Race:
                        subcategory = try decoder.decode(Race.self, from: data)
                    case .Equipment:
                        subcategory = try decoder.decode(EquipmentItem.self, from: data)

                    default:
                        subcategory = try decoder.decode(Subsection.self, from: data)
                    }
                    completion(subcategory, nil)
                } catch let DecodingError.dataCorrupted(context) {
                    print(context)
                    completion(nil, nil)
                } catch let DecodingError.keyNotFound(key, context) {
                    print("Key '\(key)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                    completion(nil, nil)
                } catch let DecodingError.valueNotFound(value, context) {
                    print("Value '\(value)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                    completion(nil, nil)
                } catch let DecodingError.typeMismatch(type, context)  {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                    completion(nil, nil)
                } catch {
                    print("error: ", error)
                    completion(nil, nil)
                }
            case .failure(let error):
                completion(nil, error)

            }
        }
    }

    func fetchClassSpells(type: ClassType, completion: @escaping (Any?, Error?)-> ()) {

        let endpoint:String = "\(self.baseURL)/classes/\(type.getIndex())/spells"
        guard let url = URL(string: endpoint) else {
            completion(nil, nil)
            return
        }
        AF.request(url, method: .get).response { response in
            switch response.result {
            case .success(let data):
                let decoder = JSONDecoder()
                guard let data = data else { return }
                do {
                    let category = try decoder.decode(Category.self, from: data)
                    let subcategory = category.results
                    completion(subcategory, nil)
                } catch let DecodingError.dataCorrupted(context) {
                    print(context)
                    completion(nil, nil)
                } catch let DecodingError.keyNotFound(key, context) {
                    print("Key '\(key)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                    completion(nil, nil)
                } catch let DecodingError.valueNotFound(value, context) {
                    print("Value '\(value)' not found:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                    completion(nil, nil)
                } catch let DecodingError.typeMismatch(type, context)  {
                    print("Type '\(type)' mismatch:", context.debugDescription)
                    print("codingPath:", context.codingPath)
                    completion(nil, nil)
                } catch {
                    print("error: ", error)
                    completion(nil, nil)
                }
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
}
