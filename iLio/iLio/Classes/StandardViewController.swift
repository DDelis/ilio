//
//  StandardViewController.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import UIKit

class StandardViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Change This Title"
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("-> \(self) got loaded")
    }
    
    deinit{
        print("-> \(self) got deinitialized")
    }

}
