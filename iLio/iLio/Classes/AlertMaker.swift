//
//  AlertMaker.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import Foundation
import UIKit

public class AlertMaker {
    
    static func makeErrorAlert(_ error: AlertType) -> UIAlertController {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        switch error {
        case .NotImplementedYet:
            alert.title = "Hold on"
            alert.message = "This feature hasn't been implemented yet"
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        case .ApiNotResponding:
            alert.title = "Oops"
            alert.message = "Api is not responding either server is busy or your connection is"
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        case .CharacterAlreadyExists:
            alert.title = "Oops"
            alert.message = "Character with this specific name already exists"
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        case .EmptyPassword:
            alert.title = "Oops"
            alert.message = "Password is empty, please try again"
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        case .NonValidCredentials:
            alert.title = "Oops"
            alert.message = "Wrong Password/Username, please try again"
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        }
        
        return alert
    }

    static func makeConfirmAlert(_ type: ConfirmAlertType, action: UIAlertAction) -> UIAlertController {

        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        switch type {
        case .confirmCharacter:
            alert.title = "Are you sure?"
            alert.message = "Is this the character you want?"
            alert.addAction((action))
            alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: nil))
        case .confirmMap:
            alert.title = "Are you sure"
            alert.message = "Is this the battlefield you want?"
            alert.addAction(action)
            alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: nil))
        case .confirmParty:
            alert.title = "Are you sure"
            alert.message = "Is this the party you want?"
            alert.addAction(action)
            alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: nil))
        case .confirmDeleteUserDefaultsAll:
            alert.title = "Are you sure"
            alert.message = "This action is irreversible"
            alert.addAction(action)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        }
        return alert
    }
}
