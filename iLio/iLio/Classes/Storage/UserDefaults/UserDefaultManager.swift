//
//  UserDefaultManager.swift
//  iLio
//
//  Created by Dimitris Delis on 28/6/21.
//

import Foundation

public class UserDefaultManager {

    static func decodePlayerCharacter(_ objectData: Data) -> [PlayerCharacter]? {
        let jsonDecoder = JSONDecoder()
        if let object = try? jsonDecoder.decode([PlayerCharacter].self, from: objectData) {
            return object
        } else {
            return nil
        }
    }


    static func encodePlayerCharacters(_ object: [PlayerCharacter]) -> Data? {
        let jsonEncoder = JSONEncoder()
        if let object = try? jsonEncoder.encode(object) {
            return object
        } else {
            return nil
        }
    }

    static func getCharacterFromUserDefaults() -> [PlayerCharacter]? {
        let defaults: UserDefaults = UserDefaults()
        if let characters = defaults.data(forKey: "PlayerCharacters") {
            return UserDefaultManager.decodePlayerCharacter(characters)
        } else {
            return nil
        }
    }

    static func saveCharacterToUserDefaults(_ character: PlayerCharacter, add: Bool) -> Bool {
        let defaults: UserDefaults = UserDefaults()
        var array:[PlayerCharacter] = []
        if let playerCharacters = self.getCharacterFromUserDefaults() {
            array = playerCharacters
        }
        var index:Int = 0
        if !array.isEmpty {
            for i in 0...array.count-1 {
                if array[i] == character {
                    if add {
                        // don't add a character if he already exists
                        return false
                    } else {
                        index = i
                    }
                }
            }
        }
        if add {
            array.append(character)
        } else {
            array.remove(at: index)
        }
        if let data = UserDefaultManager.encodePlayerCharacters(array) {
            defaults.setValue(data, forKey: "PlayerCharacters")
            return true
        } else {
            return false
        }
    }

    static func deleteAllCharactersFromUsersDefaults() {
        let defaults: UserDefaults = UserDefaults()
        defaults.removeObject(forKey: "PlayerCharacters")
    }

    static func decodeMap(_ objectData: Data) -> [CodableMap]? {
        let jsonDecoder = JSONDecoder()
        if let object = try? jsonDecoder.decode([CodableMap].self, from: objectData) {
            return object
        } else {
            return nil
        }
    }


    static func encodeMap(_ object: [CodableMap]) -> Data? {
        let jsonEncoder = JSONEncoder()
        if let object = try? jsonEncoder.encode(object) {
            return object
        } else {
            return nil
        }
    }

    static func saveMapToUserDefaults(_ map: CodableMap, add: Bool) -> Bool {
        let defaults: UserDefaults = UserDefaults()
        var array:[CodableMap] = []
        if let maps = self.getMapsFromUserDefaults() {
            array = maps
        }
        var index:Int = 0
        if !array.isEmpty {
            for i in 0...array.count-1 {
                if array[i] == map {
                    if add {
                        // don't add a character if he already exists
                        return false
                    } else {
                        index = i
                    }
                }
            }
        }
        if add {
            array.append(map)
        } else {
            array.remove(at: index)
        }
        if let data = UserDefaultManager.encodeMap(array) {
            defaults.setValue(data, forKey: "Maps")
            return true
        } else {
            return false
        }
    }

    static func getMapsFromUserDefaults() -> [CodableMap]? {
        let defaults: UserDefaults = UserDefaults()
        if let maps = defaults.data(forKey: "Maps") {
            return UserDefaultManager.decodeMap(maps)
        } else {
            return nil
        }
    }

    static func deleteMapsFromUserDefaults(){
        let defaults: UserDefaults = UserDefaults()
        defaults.removeObject(forKey: "Maps")
    }

    static func savePlayerPartyToUserDefaults(_ party: CharacterParty, add: Bool) -> Bool {
        let defaults: UserDefaults = UserDefaults()
        var array:[CharacterParty] = []
        if let parties = self.getPartiesFromUserDefaults() {
            array = parties
        }
        var index:Int = 0
        if !array.isEmpty {
            for i in 0...array.count-1 {
                if array[i] == party {
                    if add {
                        // don't add a character if he already exists
                        return false
                    } else {
                        index = i
                    }
                }
            }
        }
        if add {
            array.append(party)
        } else {
            array.remove(at: index)
        }
        if let data = UserDefaultManager.encodeParty(array) {
            defaults.setValue(data, forKey: "Parties")
            return true
        } else {
            return false
        }
    }

    static func getPartiesFromUserDefaults() -> [CharacterParty]? {
        let defaults: UserDefaults = UserDefaults()
        if let parties = defaults.data(forKey: "Parties") {
            return UserDefaultManager.decodeParty(parties)
        } else {
            return nil
        }
    }

    static func decodeParty(_ objectData: Data) -> [CharacterParty]? {
        let jsonDecoder = JSONDecoder()
        if let object = try? jsonDecoder.decode([CharacterParty].self, from: objectData) {
            return object
        } else {
            return nil
        }
    }


    static func encodeParty(_ object: [CharacterParty]) -> Data? {
        let jsonEncoder = JSONEncoder()
        if let object = try? jsonEncoder.encode(object) {
            return object
        } else {
            return nil
        }
    }

    static func deletePartiesFromUserDefaults() {
        let defaults: UserDefaults = UserDefaults()
        defaults.removeObject(forKey: "Parties")
    }

}
