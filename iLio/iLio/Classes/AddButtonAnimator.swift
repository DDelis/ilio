
//
//  ContentView.swift
//  SwiftUI-Animations
//
//  Created by Shubham Singh on 05/08/20.
//  Copyright © 2020 Shubham Singh. All rights reserved.
//

import SwiftUI

enum CartState {
    case ready
    case center
    case moveToEnd
    case origin

    var offset: CGFloat {
        switch self {
        case .origin, .ready:
            return  -(UIScreen.main.bounds.width / 2 - 80)
        case .center:
            return -4
        case .moveToEnd:
            return (UIScreen.main.bounds.width / 2 + 42)
        }
    }

    var image: String {
        switch self {
        case .origin, .ready:
            return "cart"
        default:
            return "cart-fill"
        }
    }

    var animation: Animation {
        switch self {
        case .origin:
            return Animation.linear(duration: 0)
        default:
            return Animation.easeIn(duration: 0.55).delay(0.25)
        }
    }
}

struct AddCartView: View {
    // MARK:- variables
    @State var isAnimating: Bool = false
    @State var addItem: Bool = false
    @State var cartAnimation: CartState
    @State var bounceAnimation: Bool = false

    var backgroundColor: Color
    var color: Color

    // MARK:- views
    var body: some View {
        ZStack {
            self.backgroundColor
                .edgesIgnoringSafeArea(.all)
            ZStack {
                self.color
                CartView(itemAdded: $addItem, animation: self.cartAnimation.animation)
                    .offset(x: self.cartAnimation.offset)
                    .scaleEffect(self.isAnimating ? 1.1 : 1)
                    .animation(Animation.linear(duration: 0.5).delay(0.25))


                Text("Add to cart")
                    .foregroundColor(self.backgroundColor)
                    .font(.system(size: 24, weight: .bold, design: .rounded))
                    .opacity(self.isAnimating ? 0 : 1)
                    .animation(Animation.spring())

            }.frame(height: 72)
            .cornerRadius(12)
            .padding()
            .padding([.leading, .trailing], 24)
            .shadow(radius: 10)
            .scaleEffect(x: self.bounceAnimation ? 0.98 : 1, y: 1, anchor: .center)
            .animation(
                Animation.spring(response: 0.25, dampingFraction: 0.85, blendDuration: 1)
                    .delay(0.15)
            )

            .onAppear() {
                self.cartAnimation = .center
                self.isAnimating.toggle()
                self.bounceAnimation.toggle()

                Timer.scheduledTimer(withTimeInterval: 0.7, repeats: false) { (Timer) in
                    self.bounceAnimation.toggle()
                }

                Timer.scheduledTimer(withTimeInterval: 1.4, repeats: false) { (Timer) in
                    self.addItem.toggle()
                }
                Timer.scheduledTimer(withTimeInterval: 1.6, repeats: false) { (Timer) in
                    self.cartAnimation = .moveToEnd
                }
                Timer.scheduledTimer(withTimeInterval: 2.5, repeats: false) { (Timer) in
                    self.isAnimating.toggle()
                    self.addItem.toggle()
                    self.cartAnimation = .origin
                }

            }

            Triangle()
                .fill(Color.white)
                .frame(width: 120, height: self.bounceAnimation ? 22 : 0)
                .animation(
                    Animation.spring(response: 0.25, dampingFraction: 0.85, blendDuration: 1)
                        .delay(0.35)
                )
                .offset(y: -36)
            if (self.isAnimating) {
                ShirtView(itemAdded: $isAnimating)
                    .frame(width: 22, height: 22)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            AddCartView(cartAnimation: .ready, backgroundColor: Color.black, color: Color.white)
        }
    }
}

struct CartView: View {

    @Binding var itemAdded: Bool

    var animationDuration: Double = 0.55
    var animationDelay: Double = 0.25
    var animation: Animation

    var body: some View {
        ZStack {
            Image(self.itemAdded ? "cart-fill" : "cart")
                .resizable()
                .frame(width: 42, height: 42)
                .rotationEffect(self.itemAdded ? .degrees(-22) : .degrees(0))
                .animation(self.animation)
            Tick(scaleFactor: 0.125)
                .trim(from: 0, to: self.itemAdded ? 1 : 0)
                .stroke(style: StrokeStyle(lineWidth: 2.4, lineCap: .round))
                .foregroundColor(Color.red)
                .frame(width: 42, height: 42)
                .animation(.easeOut(duration: 0.35))
                .rotationEffect(self.itemAdded ? .degrees(-22) : .degrees(0))
                .animation(Animation.easeIn(duration: self.animationDuration).delay(self.animationDelay))
        }.onTapGesture {
            self.itemAdded.toggle()
        }
    }
}



struct CartView_Previews: PreviewProvider {
    static var previews: some View {
        CartView(itemAdded: .constant(true), animation: Animation.easeIn(duration: 0.55).delay(0.25))
    }
}

enum ShirtState {
    case origin
    case top
    case end

    var scale: CGFloat {
        switch self {
        case .origin:
            return 0.2
        case .top:
            return 1
        case .end:
            return 0.5
        }
    }

    var opacity: Double {
        switch self {
        case .end:
            return 0
        default:
            return 1
        }
    }
}

struct ShirtView: View {
    @Binding var itemAdded: Bool

    @State var iconOffset: CGFloat = -12
    @State var changeShirtColor: Bool = false
    @State var shirtState: ShirtState = .origin


    var body: some View {
        ZStack {
            Color.clear
                .edgesIgnoringSafeArea(.all)
            Image(self.changeShirtColor ? "shirt-black" : "shirt-white")
                .resizable()
                .frame(width: 27, height: 27)
                .scaleEffect(self.itemAdded ? self.shirtState.scale : 0.5)
                .animation(Animation.spring().speed(0.6))
                .offset(y: self.iconOffset)
                .animation(Animation.easeOut(duration: 0.35))
                .opacity(self.shirtState.opacity)
                .animation(Animation.default.delay(0.15))
                .onAppear() {
                    Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { checkingTimer in
                        if (self.itemAdded) {
                            checkingTimer.invalidate()
                            Timer.scheduledTimer(withTimeInterval: 0.35, repeats: false) { (Timer) in
                                self.iconOffset = -120
                                self.shirtState = .top
                            }
                            Timer.scheduledTimer(withTimeInterval: 1.25, repeats: false) { (Timer) in
                                self.changeShirtColor.toggle()
                                self.shirtState = .end
                            }
                            Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (Timer) in
                                self.iconOffset = -6
                            }
                        }
                    }
            }
        }
    }
}

struct ShirtView_Previews: PreviewProvider {
    static var previews: some View {
        ZStack {
            Color.gray
                .edgesIgnoringSafeArea(.all)
            ShirtView(itemAdded: .constant(false))
        }
    }
}

struct Tick: Shape {
    let scaleFactor: CGFloat

    func path(in rect: CGRect) -> Path {
        let cX = rect.midX + 4
        let cY = rect.midY - 3

        var path = Path()
        path.move(to: CGPoint(x: rect.midX, y: rect.minY))
        path.move(to: CGPoint(x: cX - (42 * scaleFactor), y: cY - (4 * scaleFactor)))
        path.addLine(to: CGPoint(x: cX - (scaleFactor * 18), y: cY + (scaleFactor * 28)))
        path.addLine(to: CGPoint(x: cX + (scaleFactor * 40), y: cY - (scaleFactor * 36)))
        return path
    }
}

struct Tick_Previews: PreviewProvider {
    static var previews: some View {
        Tick(scaleFactor: 1)
    }
}

struct Triangle: Shape {
    func path(in rect: CGRect) -> Path {
        var path = Path()

        path.move(to: CGPoint(x: rect.midX, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.minX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.midX, y: rect.minY))

        return path
    }
}

struct Triangle_Previews: PreviewProvider {
    static var previews: some View {
        Triangle()
    }
}
