//
//  OutletAnimationManager.swift
//  iLio
//
//  Created by Dimitris Delis on 14/6/21.
//

import Foundation
import UIKit

public class OutletAnimationManager {
    
    static func animateButtonAppearanceRightToCenter(_ button: UIButton) {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseIn) {
            button.frame = CGRect(x: UIScreen.main.bounds.width, y: button.frame.origin.y, width: button.frame.width, height: button.frame.height)
        }
    }
    
    static func animateTextfieldAppearanceLeftToCenter(_ textfield: UITextField) {
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseIn) {
            textfield.frame = CGRect(x: -UIScreen.main.bounds.width, y: textfield.frame.origin.y, width: textfield.frame.width, height: textfield.frame.height)
        }
    }

    static func animateButtonPressed(_ sender: UIButton, handler: @escaping () -> ()) {
        UIView.animate(withDuration: 0.6,
                       animations: {
                        sender.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                       },
                       completion: { _ in
                        UIView.animate(withDuration: 0.6) {
                            sender.transform = CGAffineTransform.identity
                        }
                        handler()
                       })
    }

    
}
