//
//  AlertMakerTests.swift
//  iLioTests
//
//  Created by Dimitris Delis on 13/7/21.
//

import XCTest
@testable import iLio

class AlertMakerTests: XCTestCase {

    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func testAlertMaker_WhenTypeIsErrorType() {

        // Given an array of error types
        let errors:[AlertType] = [.ApiNotResponding, .CharacterAlreadyExists, .EmptyPassword , .NonValidCredentials, .NotImplementedYet]

        // When fed to an alert maker
        // Then the result alert controller will have the correct title/message
        for error in errors {
            let vc = AlertMaker.makeErrorAlert(error)
            switch error {
            case .EmptyPassword:
                XCTAssert(vc.title == "Oops", "title is set wrong")
                XCTAssert(vc.message == "Password is empty, please try again", "message is set wrong")
            case .NonValidCredentials:
                XCTAssert(vc.title == "Oops", "title is set wrong")
                XCTAssert(vc.message == "Wrong Password/Username, please try again", "message is set wrong")
            case .NotImplementedYet:
                XCTAssert(vc.title == "Hold on", "title is set wrong")
                XCTAssert(vc.message == "This feature hasn't been implemented yet", "message is set wrong")
            case .ApiNotResponding:
                XCTAssert(vc.title == "Oops", "title is set wrong")
                XCTAssert(vc.message == "Api is not responding either server is busy or your connection is", "message is set wrong")
            case .CharacterAlreadyExists:
                XCTAssert(vc.title == "Oops", "title is set wrong")
                XCTAssert(vc.message == "Character with this specific name already exists", "message is set wrong")
            }
        }
    }

    func testAlertMaker_WhenTypeIsConfirmType() {

        // Given an array of confirm types and an action
        let confirms:[ConfirmAlertType] = [.confirmCharacter, .confirmMap, .confirmParty, .confirmDeleteUserDefaultsAll]
        let action: MockAlertAction = MockAlertAction(title: "randomtext", style: .default, handler: nil)

        // When fed to an alert maker
        // Then the result alert controller will have the correct title/message
        for confirm in confirms {
            let vc = AlertMaker.makeConfirmAlert(confirm, action: action)
            switch confirm {
            case .confirmCharacter:
                XCTAssert(vc.title == "Are you sure?", "title is set wrong")
                XCTAssert(vc.message == "Is this the character you want?", "message is set wrong")
            case .confirmMap:
                XCTAssert(vc.title == "Are you sure", "title is set wrong")
                XCTAssert(vc.message == "Is this the battlefield you want?", "message is set wrong")
            case .confirmParty:
                XCTAssert(vc.title == "Are you sure", "title is set wrong")
                XCTAssert(vc.message == "Is this the party you want?", "message is set wrong")
            case .confirmDeleteUserDefaultsAll:
                XCTAssert(vc.title == "Are you sure", "title is set wrong")
                XCTAssert(vc.message == "This action is irreversible", "message is set wrong")
            }
        }
    }

}

class MockAlertAction: UIAlertAction {
}
